**********************************************************************************************************************************************************
**
** @package Computing Project, 2014
** @author Nathan Day, K0028502, k0028502@tees.ac.uk
** @link https://k0028502.scm.tees.ac.uk/yii/ComputingProject/index.php/
**
**********************************************************************************************************************************************************
                                                                                                                                                          
                                                                                                                                                          
RRRRRRRRRRRRRRRRR   EEEEEEEEEEEEEEEEEEEEEE               AAA               DDDDDDDDDDDDD             MMMMMMMM               MMMMMMMMEEEEEEEEEEEEEEEEEEEEEE
R::::::::::::::::R  E::::::::::::::::::::E              A:::A              D::::::::::::DDD          M:::::::M             M:::::::ME::::::::::::::::::::E
R::::::RRRRRR:::::R E::::::::::::::::::::E             A:::::A             D:::::::::::::::DD        M::::::::M           M::::::::ME::::::::::::::::::::E
RR:::::R     R:::::REE::::::EEEEEEEEE::::E            A:::::::A            DDD:::::DDDDD:::::D       M:::::::::M         M:::::::::MEE::::::EEEEEEEEE::::E
  R::::R     R:::::R  E:::::E       EEEEEE           A:::::::::A             D:::::D    D:::::D      M::::::::::M       M::::::::::M  E:::::E       EEEEEE
  R::::R     R:::::R  E:::::E                       A:::::A:::::A            D:::::D     D:::::D     M:::::::::::M     M:::::::::::M  E:::::E             
  R::::RRRRRR:::::R   E::::::EEEEEEEEEE            A:::::A A:::::A           D:::::D     D:::::D     M:::::::M::::M   M::::M:::::::M  E::::::EEEEEEEEEE   
  R:::::::::::::RR    E:::::::::::::::E           A:::::A   A:::::A          D:::::D     D:::::D     M::::::M M::::M M::::M M::::::M  E:::::::::::::::E   
  R::::RRRRRR:::::R   E:::::::::::::::E          A:::::A     A:::::A         D:::::D     D:::::D     M::::::M  M::::M::::M  M::::::M  E:::::::::::::::E   
  R::::R     R:::::R  E::::::EEEEEEEEEE         A:::::AAAAAAAAA:::::A        D:::::D     D:::::D     M::::::M   M:::::::M   M::::::M  E::::::EEEEEEEEEE   
  R::::R     R:::::R  E:::::E                  A:::::::::::::::::::::A       D:::::D     D:::::D     M::::::M    M:::::M    M::::::M  E:::::E             
  R::::R     R:::::R  E:::::E       EEEEEE    A:::::AAAAAAAAAAAAA:::::A      D:::::D    D:::::D      M::::::M     MMMMM     M::::::M  E:::::E       EEEEEE
RR:::::R     R:::::REE::::::EEEEEEEE:::::E   A:::::A             A:::::A   DDD:::::DDDDD:::::D       M::::::M               M::::::MEE::::::EEEEEEEE:::::E
R::::::R     R:::::RE::::::::::::::::::::E  A:::::A               A:::::A  D:::::::::::::::DD        M::::::M               M::::::ME::::::::::::::::::::E
R::::::R     R:::::RE::::::::::::::::::::E A:::::A                 A:::::A D::::::::::::DDD          M::::::M               M::::::ME::::::::::::::::::::E
RRRRRRRR     RRRRRRREEEEEEEEEEEEEEEEEEEEEEAAAAAAA                   AAAAAAADDDDDDDDDDDDD             MMMMMMMM               MMMMMMMMEEEEEEEEEEEEEEEEEEEEEE
                                                                                                                                                          

**********************************************************************************************************************************************************


  ___               _                       _      
 | _ \___ __ _ _  _(_)_ _ ___ _ __  ___ _ _| |_ ___
 |   / -_) _` | || | | '_/ -_) '  \/ -_) ' \  _(_-<
 |_|_\___\__, |\_,_|_|_| \___|_|_|_\___|_||_\__/__/
            |_|                                    
  In order to correctly run the web application included on this CD/DVD you will need to upload the files to an apache web server running at least 
  PHP 5.10 and a MySQL database. Further requirements may need to be met. in order to check these follow the install instructions and navigate to 
  http(s)://hostname/path/to/yii/requirements/index.php. Before starting the installation and the initial setup of the application it is best to 
  know the details of your web server and database server. These may be the same but if you are in doubt please contact you hosting server 
  administrator. Things that you will need to know are:
    MySQL Host Name
    Database Name
    Database Username
    Databaase Password


**********************************************************************************************************************************************************


  ___         _        _ _   ___         _               _   _             
 |_ _|_ _  __| |_ __ _| | | |_ _|_ _  __| |_ _ _ _  _ __| |_(_)___ _ _  ___
  | || ' \(_-<  _/ _` | | |  | || ' \(_-<  _| '_| || / _|  _| / _ \ ' \(_-<
 |___|_||_/__/\__\__,_|_|_| |___|_||_/__/\__|_|  \_,_\__|\__|_\___/_||_/__/

  In order to install the files all that you need to do is simply copy the files from the CD/DVD to a correctly configured apache webserver. 
  for best practise copy the Yii folder and all of its contents from this CD/DVD to the root of the server. Once the files have been correctly 
  copied to the server please navigate to http(s)://hostname/path/to/yii/ComputingProject/index.php. the index.php is not necessary as the app will 
  automatically redirect you however it is best practise to type this the first time. 


**********************************************************************************************************************************************************


  _   _    _             _   _            _             _ _         _   _          
 | | | |__(_)_ _  __ _  | |_| |_  ___    /_\  _ __ _ __| (_)__ __ _| |_(_)___ _ _  
 | |_| (_-< | ' \/ _` | |  _| ' \/ -_)  / _ \| '_ \ '_ \ | / _/ _` |  _| / _ \ ' \ 
  \___//__/_|_||_\__, |  \__|_||_\___| /_/ \_\ .__/ .__/_|_\__\__,_|\__|_\___/_||_|
                 |___/                       |_|  |_|                              
    To correctly use the application once installed navigate to http(s)://hostname/path/to/yii/ComputingProject/index.php. this will automatically 
    redirect you to the Application Setup pages. in the main body of the page you will see three sections Welcome, Database and Modules. read the Welcome 
    text then continue onto the next sections by pressing the next button in the bottom right hand side of the screen. In order to Run this application 
    inside the protected/config/ folder there are three main.php files, main.php, main.php.backup and main.php.complete. main.php is the file that is 
    currently being used by the application and will be edited with the given database details, main.php.backup is a backup of the original main.php with 
    no editing and main.php.complete is a file already fileld out with my details connected to my test database.

    DATABSE SETUP:
      Once at the Database screen you will be prompted to input some information. Work your way through the page inputting the requested information. 
      if you get to a section that you are not sure about STOP. before advancing any further contact you host Administrator or customer service to find 
      the information that you need. for a list of required information please see the requirements section above. 
      Once all the information is inputted press the button labelled Submit Database Details. This will Validate you inputted information and return any 
      errors if the details were incorrect. If errors are returned please correct the affected fields and try again.

    USER SETUP:
      Once the Database details have been saved and the Database is setup you will notice another section appear between Database and Modules, This 
      Section asks you for some information in order to setup the First Administrator of your bespoke application. please fill in the requested information
      and make a note of the information given. any passwords entered are securely saved and are not visible in plain text once this process is completed. 

    MODULE SETUP:
      Once a Database and Administrator have been setup you can continue onto the next stage choosing the required modules to setup you bespoke web 
      application. The current modules available to you just an example of what can be chosen and are not complete modules at this stage. however Choosing 
      a module by selecting it and pressing save will grab the necessary files from the modules folder and place them in the necessary places in order to 
      start using the application. Once the Database, User and Modules have been setup you will automatically redirect to the main page of the website. 
      To check that a module has correctly been setup you can navigate to http(s)://hostname/path/to/yii/ComputingProject/index.php/moduleName. 


**********************************************************************************************************************************************************


    _      _                         _   ___         _                   
   /_\  __| |_ ____ _ _ _  __ ___ __| | | __|__ __ _| |_ _  _ _ _ ___ ___
  / _ \/ _` \ V / _` | ' \/ _/ -_) _` | | _/ -_) _` |  _| || | '_/ -_|_-<
 /_/ \_\__,_|\_/\__,_|_||_\__\___\__,_| |_|\___\__,_|\__|\_,_|_| \___/__/
                                                                         
  This project has been designed as a starting platform for other developers to utilise in order to create and manage bespoke applications for their 
  clients. The Aim of this and future developments is for developers to add their own modules to the starting platform edit the necessary style sheets and
  ship out the product to their clients with the minimal amount of coding and repetition, drastically cutting down development times for bespoke 
  applications and increasing profitability. 

  In order to take advantage of these advanced features developers need to create a new folder within the gii/Modules folder and include some basic files.
  full path to mentioned directory is //hostname/path/to/yii/ProjectTitle/protected/gii/modules. I would recommend anyone wanting to use these advanced 
  features to look at the current included modules to fully understand the modular system. In each module folder there are three folders, controllers, sql
  and views along with a single file called tables.php. developers must also be aware of a file in //hostname/path/to/yii/ProjectTitle/protected/gii/ 
  called modules.php. To add a module to the application a developer must include as a minimum the tables.php and any sql files necessary to generate the
  tables required for that module.

    ** Best Practise - if an SQL file is called upon more than once rather than having duplicates, drop this file into Common/sql inside the modules 
    folder. The application will first of all look for the SQL file in the module folder and if it cannot be found will then check in the common folder.

  The required SQL files MUST be one file per table and are recommended to be written using the INNODB engine of Mysql. A template for this and other 
  files have been included in the folder Templates within the module folder. 

    ** NOTE this is not a module of its own this is just a folder of templates for developers to use in order to create files that follow the same schema
       as files that are already included.

  Once the required SQL files and Tables file has been created a Developer must add the module to the Modules list inside modules.php, this will ensure
  that the module is included in the options list when a user sets up the application for the first time. if only the minimum files have been included 
  once the module has been selected from the modules list the models and CRUD files will automatically be generated using the built in Gii module from 
  Yii. this Module has been slightly altered and improved so that tables and/or models with multiple primary keys, AKA Composite keys, will still 
  automatically generate the CRUD unlike the original Gii functionality. A Developer can choose to include bespoke CRUD and models inside the module 
  folder in which case the setup process will grab these files and insert them into the required folders in order to be run as part of the application.
  It may be an idea for the developers to first run the application in a test environment with no bespoke CRUD or models then to alter the generated files
  in order to create their required functionality. once this has been done then move the bespoke files into the module folder ready for deployment.

  Another advanced feature included in the setup process is affected by a constant declared inside the setupController.php file. this constant is 
  currently set to false but setting this to true will skip the checking for bespoke CRUD and models inside the module folder and automatically generate 
  the required files and folder structures within the application using the bespoke Gii module included with this project. 

