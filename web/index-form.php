<?php
	date_default_timezone_set('Europe/London');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Install Your Bespoke Web Application</title>
	</head>
	<body>
		<h1>Web Application Setup</h1>
		<form action="install.php" method="post">
			<h2>Database Details<h2>
			Database Location: <input type="text" name="dbLocation" value="localhost"> </br>
			Database Name: <input type="text" name="dbName"> </br>
			User name: <input type="text" name="dbName"> </br>
			Password: <input type="password" name="dbPassword"> </br>
			<h2>Administrator Details</h2>
			User name: <input type="text" name="username" value="Admin"> </br>
			First Name: <input type="text" name="firstname"> </br>
			Surname: <input type="text" name="surname"> </br>
			Password: <input type="password" name="password1"> </br>
			Confirm Password: <input type="text" name="passwordConfirm"> </br>
			Email Address: <input type="text" name="email"> </br>
		</form>
	</body>
</html> 