<?php
	$form = $this->beginWidget(
		'booster.widgets.TbActiveForm',
		array(
			'id' => 'databaseForm',
			'htmlOptions' => array('class' => 'well'), // for inset effect
		)
	);
	echo $form->errorSummary($model);
	echo $form->textFieldGroup($model, 'username');
	echo $form->textFieldGroup($model, 'firstName');
	echo $form->textFieldGroup($model, 'lastName');
	echo $form->passwordFieldGroup($model, 'password1');
	echo $form->passwordFieldGroup($model, 'confirmPassword');
	echo $form->textFieldGroup($model, 'email');
	$this->widget(
		'booster.widgets.TbButton',
		array('buttonType' => 'submit', 'label' => 'Save')
	);
	$this->endWidget();
?>