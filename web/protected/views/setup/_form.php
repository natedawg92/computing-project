<div class="form">
<?php 
switch ($form){
	case 'Database': $form=$this->beginWidget('CActiveForm', array(
			'id'=>'database-form',
			'enableAjaxValidation'=>false,
			)); ?>
		<p class="note">Fields with <span class="required">*</span> are required.</p>
		<?php echo $form->errorSummary($model); ?>
		<div class="row">
			<?php echo $form->labelEx($model,'host'); ?>
			<?php echo $form->textField($model,'host'); ?>
			<?php echo $form->error($model,'host'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'dbname'); ?>
			<?php echo $form->textField($model,'dbname'); ?>
			<?php echo $form->error($model,'dbname'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username'); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password'); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'confirmPassword'); ?>
			<?php echo $form->passwordField($model,'confirmPassword'); ?>
			<?php echo $form->error($model,'confirmPassword'); ?>
		</div>
		<div class="row buttons">
			<?php echo CHtml::submitButton('Save'); ?>
		</div>
		<?php $this->endWidget();
	break;
	case 'User' : $form=$this->beginWidget('CActiveForm', array(
			'id'=>'user-form',
			'enableAjaxValidation'=>false,
			)); ?>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		<?php echo $form->errorSummary($model); ?>
		<div class="row">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username'); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'firstName'); ?>
			<?php echo $form->textField($model,'firstName'); ?>
			<?php echo $form->error($model,'firstName'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'lastName'); ?>
			<?php echo $form->textField($model,'lastName'); ?>
			<?php echo $form->error($model,'lastName'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'password1'); ?>
			<?php echo $form->passwordField($model,'password1'); ?>
			<?php echo $form->error($model,'password1'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'confirmPassword'); ?>
			<?php echo $form->passwordField($model,'confirmPassword'); ?>
			<?php echo $form->error($model,'confirmPassword'); ?>
		</div>
		<div class="row">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email'); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
		<div class="row buttons">
			<?php echo CHtml::submitButton('Save'); ?>
		</div>
		<?php $this->endWidget();
	break;
	case 'Modules' : $form=$this->beginWidget('CActiveForm', array(
			'id'=>'user-form',
			'enableAjaxValidation'=>false,
			)); ?>
			<p class="note">Fields with <span class="required">*</span> are required.</p>
		<?php echo $form->errorSummary($model); ?>
		<div class="row">
			<?php echo $form->labelEx($model,'modules'); ?>
			<?php echo $form->checkBoxList($model, 'modules' ,$array); ?>
			<?php echo $form->error($model,'modules'); ?>
		</div>
		<div class="row buttons">
			<?php echo CHtml::submitButton('Save'); ?>
		</div>
		<?php $this->endWidget();
	break;
	default : ;
}
 ?>
</div><!-- form -->