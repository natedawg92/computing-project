<?php
	yii::app()->clientScript->registerScript(
		'AjaxFormSubmission',
		'$("#dbSubmit").click(function(){
			var formdata = $("#databaseForm").serializeArray();
			$.ajax({
				url: "/submit_url",
				data: formdata,
				success: function(RESPONSE){
					//Your code to modify page content after geting RESPONSE.
				}
			})
    		return false; //To prevent page from reloading
		}', 
		CClientScript::POS_HEAD
		);

	$form = $this->beginWidget(
		'booster.widgets.TbActiveForm',
		array(
			'id' => 'databaseForm',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array(
				'class' => 'well', // for inset effect
				'onsubmit'=>'send();'
				),
		)
	);

	echo $form->errorSummary($model);
	echo $form->textFieldGroup($model, 'host');
	echo $form->textFieldGroup($model, 'dbname');
	echo $form->textFieldGroup($model, 'username');
	echo $form->passwordFieldGroup($model, 'password');
	echo $form->passwordFieldGroup($model, 'confirmPassword');
	$this->widget(
		'booster.widgets.TbButton',
		array(
			'context' => 'primary',
			'label' => 'Submit Database Details',
			'buttonType' => 'ajaxSubmit',
			'id'=>'dbSubmit'
		)
	);
	$this->endWidget();
?>