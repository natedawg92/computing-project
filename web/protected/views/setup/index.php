<?php
/* @var $this SetupController */

$this->breadcrumbs=array(
	'Application Setup',
);
?>

<h1>Application Setup</h1>
<?php
	$this->widget(
		'booster.widgets.TbWizard',
		array(
			'type' => 'tabs', // 'tabs' or 'pills'
			'pagerContent' => '<div style="float:right">
				<input type="button" class="btn button-next" name="next" value="Next" />
				</div>
				<div style="float:left">
				<input type="button" class="btn button-previous" name="previous" value="Previous" />
				</div><br /><br />',
			'options' => array(
				'nextSelector' => '.button-next',
				'previousSelector' => '.button-previous',
				'onTabShow' => 'js:function(tab, navigation, index) {
					var $total = navigation.find("li").length;
					var $current = index+1;
					var $percent = ($current/$total) * 100;
					$("#wizard-bar > .progress-bar").css({width:$percent+"%"});
				}',
			'onTabClick' => 'js:function(tab, navigation, index) {alert("Please Navigate using the Next and Previous Buttons Below");return false;}',
			),
			'tabs' => $tabs,
		)
	); 
?>
<div id="wizard-bar" class="progress progress-striped active">
	<div class="progress-bar" style="width: 0"></div>
</div>