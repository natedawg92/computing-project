<?php
	$form = $this->beginWidget(
		'booster.widgets.TbActiveForm',
		array(
			'id' => 'databaseForm',
			'htmlOptions' => array('class' => 'well'), // for inset effect
		)
	);
	echo $form->errorSummary($model);
	echo $form->checkboxListGroup(
		$model,
		'modules',
		array(
			'widgetOptions' => array(
				'data' => $data,
				'inline'=>true
			)
		)
	);
	$this->widget(
		'booster.widgets.TbButton',
		array('buttonType' => 'submit', 'label' => 'Save')
	);
	$this->endWidget();
?>