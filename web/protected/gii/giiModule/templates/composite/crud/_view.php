<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
$views = array();
foreach ($this->tableSchema->primaryKey as $pk){
	array_push($views, array($pk => '\$data->'.$pk));
}
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $data <?php echo $this->getModelClass(); ?> */
?>

<div class="view">
	<?php 
		echo "<b>\n";
		/* echo CHtml::link(">> ", array('view','viewid'=>$data->viewid, 'categoryid'=>$data->categoryid, 'somethingotherid'=>$data->somethingotherid)); */
		echo "echo CHtml::link('>> ', array('view', array($views)));\n";
		echo "<br/> \n </b> \n";

		$count = 0;
		foreach($this->tableSchema->columns as $column){
			if($column->isPrimaryKey) continue;
			if(++$count==7) echo "\t<?php /*\n";
			echo "\t<b><?php echo CHtml::encode(\$data->getAttributeLabel('{$column->name}')); ?>:</b>\n";
			echo "\t<?php echo CHtml::encode(\$data->{$column->name}); ?>\n\t<br />\n\n";	 
		}
		if($count>=7) echo "\t*/ ?>\n";
	?>
</div>