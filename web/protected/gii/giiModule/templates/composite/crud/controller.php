<?php
/**
 * This is the template for generating a controller class file for CRUD feature for a table with Composite Primary Keys.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView(
		<?php 
			$i = 0; 
			$len = count($this->tableSchema->primaryKey);
			foreach($this->tableSchema->primaryKey as $pk){
				$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
				echo $text;
			}
		?>
	){		
		$model=$this->loadModel(
			<?php 
				$i = 0; 
			$len = count($this->tableSchema->primaryKey);
			foreach($this->tableSchema->primaryKey as $pk){
				$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
				echo $text;
			}
			?>
		);
		$this->render('view',array('model'=>$model));
	}
	
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('<?php echo $this->modelClass; ?>');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	

	public function actionCreate()
	{
	    $model=new <?php echo $this->modelClass; ?>;

	    if(isset($_POST['ajax']) && $_POST['ajax']==='<?php echo $this->class2id($this->modelClass) ?>-form')
	    {
	        echo CActiveForm::validate($model);
	        Yii::app()->end();
	    }

	    if(isset($_POST['<?php echo $this->modelClass; ?>']))
	    {
	        $model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
	        if($model->validate())
	        {
				$this->saveModel($model);
				$this->redirect(array('view', 
				<?php
					$i = 0;
					$len = count($this->tableSchema->primaryKey);
					foreach($this->tableSchema->primaryKey as $pk){
						$text = (++$i === $len-1 ? "'$pk'=>\$model->$pk, " : "'$pk'=>\$model->$pk");
						echo $text;
					}
				?>
				));
	        }
	    }
	    $this->render('create',array('model'=>$model));
	} 
	
	public function actionDelete(
		<?php 
			$i = 0; 
			$len = count($this->tableSchema->primaryKey);
			foreach($this->tableSchema->primaryKey as $pk){
				$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
				echo $text;
			}
		?>
	){
		if(Yii::app()->request->isPostRequest)
		{
			try
			{
				// we only allow deletion via POST request
				$this->loadModel(
					<?php 
						$i = 0; 
						$len = count($this->tableSchema->primaryKey);
						foreach($this->tableSchema->primaryKey as $pk){
							$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
							echo $text;
						}
					?>
				)->delete();
			}
			catch(Exception $e) 
			{
				$this->showError($e);
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionUpdate(
		<?php 
			$i = 0; 
			$len = count($this->tableSchema->primaryKey);
			foreach($this->tableSchema->primaryKey as $pk){
				$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
				echo $text;
			}
		?>
	){
		$model=$this->loadModel(
			<?php 
				$i = 0; 
				$len = count($this->tableSchema->primaryKey);
				foreach($this->tableSchema->primaryKey as $pk){
					$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
					echo $text;
				}
			?>
		);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
			$this->saveModel($model);
			$this->redirect(array('view',
				<?php
					$i = 0;
					$len = count($this->tableSchema->primaryKey);
					foreach($this->tableSchema->primaryKey as $pk){
						$text = (++$i === $len-1 ? "'$pk'=>\$model->$pk, " : "'$pk'=>\$model->$pk");
						echo $text;
					}
				?>
	        ));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionAdmin()
	{
		$model=new <?php echo $this->modelClass; ?>('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['<?php echo $this->modelClass; ?>']))
			$model->attributes=$_GET['<?php echo $this->modelClass; ?>'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function loadModel(
		<?php 
			$i = 0; 
			$len = count($this->tableSchema->primaryKey);
			foreach($this->tableSchema->primaryKey as $pk){
				$text = (++$i === $len-1 ? "\$$pk, " : "\$$pk");
				echo $text;
			}
		?>
	){
		$model=<?php echo $this->modelClass; ?>::model()->findByPk(array(
			<?php
				$i = 0;
				$len = count($this->tableSchema->primaryKey);
				foreach($this->tableSchema->primaryKey as $pk){
					$text = (++$i === $len-1 ? "'$pk'=>\$model->$pk, " : "'$pk'=>\$model->$pk");
					echo $text;
				}
			?>
		));
		if($model==null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function saveModel($model)
	{
		try
		{
			$model->save();
		}
		catch(Exception $e)
		{
			$this->showError($e);
		}		
	}

	function showError(Exception $e)
	{
		if($e->getCode()==23000)
			$message = "This operation is not permitted due to an existing foreign key reference.";
		else
			$message = "Invalid operation.";
		throw new CHttpException($e->getCode(), $message);
	}		
}