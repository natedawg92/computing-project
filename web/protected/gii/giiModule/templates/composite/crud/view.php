<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->pluralize($this->class2name($this->modelClass)); ?>', 'url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
	array('label'=>'Update <?php echo $this->modelClass; ?>', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete <?php echo $this->modelClass; ?>', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										<?php
	      											$i = 0;
	      											$len = count($this->tableSchema->primaryKey);
	      											foreach($this->tableSchema->primaryKey as $pk){
	      												if (++$i === $len-1){
	      													echo "'$pk'=>\$model->$pk";
	      												} else {
	      													echo "'$pk'=>\$model->$pk,";
	      												}
	      											}
	      										?>
	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?>', 'url'=>array('admin')),
);
?>

<h1>View <?php echo $this->modelClass; ?></h1>

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>
