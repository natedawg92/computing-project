SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Avatar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Avatar` (
  `avatarid` INT NOT NULL AUTO_INCREMENT COMMENT 'Avatar ID',
  `mimeType` VARCHAR(64) NOT NULL COMMENT 'Avatar Mime Type',
  `blobData` BLOB NOT NULL COMMENT 'Avatar Image Data',
  `public` TINYINT(1) NOT NULL COMMENT 'Available to the Public',
  PRIMARY KEY (`avatarid`),
  UNIQUE INDEX `avatarid_UNIQUE` (`avatarid` ASC))
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;