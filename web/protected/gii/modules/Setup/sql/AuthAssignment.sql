SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `AuthAssignment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AuthAssignment` (
  `itemname` VARCHAR(64) NOT NULL COMMENT 'Item Name',
  `userid` INT NOT NULL COMMENT 'User',
  INDEX `authAssignmentUser` (`userid` ASC),
  INDEX `authAssignmentAuthItem` (`itemname` ASC),
  PRIMARY KEY (`itemname`, `userid`),
  CONSTRAINT `authAssignmentUser`
    FOREIGN KEY (`userid`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `authAssignmentAuthItem`
    FOREIGN KEY (`itemname`)
    REFERENCES `AuthItem` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;