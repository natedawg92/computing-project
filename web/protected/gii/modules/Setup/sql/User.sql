SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `User` (
  `userid` INT NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `username` VARCHAR(64) NOT NULL COMMENT 'User Name',
  `firstName` VARCHAR(64) NOT NULL COMMENT 'First Name',
  `lastName` VARCHAR(64) NOT NULL COMMENT 'Surname',
  `password` VARCHAR(64) NOT NULL COMMENT 'Password',
  `email` VARCHAR(128) NOT NULL COMMENT 'Email Address',
  `homePhone` VARCHAR(11) NULL COMMENT 'Home Phone Number',
  `mobPhone` VARCHAR(11) NULL COMMENT 'Mobile Phone Number',
  `avatarID` INT NOT NULL COMMENT 'Avatar',
  `homeAddress` INT NOT NULL COMMENT 'Home Address',
  PRIMARY KEY (`userid`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `userAvatar` (`avatarID` ASC),
  INDEX `userAddress` (`homeAddress` ASC),
  CONSTRAINT `userAvatar`
    FOREIGN KEY (`avatarID`)
    REFERENCES `Avatar` (`avatarid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `userAddress`
    FOREIGN KEY (`homeAddress`)
    REFERENCES `Location` (`locationid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;