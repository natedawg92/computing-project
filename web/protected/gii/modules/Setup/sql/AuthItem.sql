SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `AuthItem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AuthItem` (
  `name` VARCHAR(64) NOT NULL COMMENT 'Item Name',
  `type` INT NOT NULL COMMENT 'Item Type',
  `description` TEXT NULL COMMENT 'Item Description',
  `bizrule` TEXT NULL COMMENT 'Business Rule',
  `data` TEXT NULL DEFAULT 'Null;' COMMENT 'Item Data',
  PRIMARY KEY (`name`))
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;