SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Location` (
  `locationid` INT NOT NULL AUTO_INCREMENT COMMENT 'Location ID',
  `name` VARCHAR(64) NOT NULL COMMENT 'Location Name',
  `addressLine1` VARCHAR(128) NOT NULL COMMENT 'Address Line One',
  `addressLine2` VARCHAR(128) NULL,
  `town` VARCHAR(128) NOT NULL COMMENT 'Town / City',
  `postCode` VARCHAR(8) NOT NULL COMMENT 'Post Code',
  `latitude` VARCHAR(11) NULL COMMENT 'Latitude',
  `longitude` VARCHAR(11) NULL COMMENT 'Logitude',
  `link` TEXT NULL,
  `mediaid` INT NOT NULL COMMENT 'Location Image',
  PRIMARY KEY (`locationid`),
  INDEX `locationImage` (`mediaid` ASC),
  CONSTRAINT `LocationImage`
    FOREIGN KEY (`mediaid`)
    REFERENCES `Media` (`mediaid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;