<?php
/* @var $this MediaController */
/* @var $data Media */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mediaid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mediaid), array('view', 'id'=>$data->mediaid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mimeType')); ?>:</b>
	<?php echo CHtml::encode($data->mimeType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('blobData')); ?>:</b>
	<?php echo CHtml::encode($data->blobData); ?>
	<br />


</div>