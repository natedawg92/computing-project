<?php
/* @var $this MediaController */
/* @var $model Media */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'mediaid'); ?>
		<?php echo $form->textField($model,'mediaid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mimeType'); ?>
		<?php echo $form->textField($model,'mimeType',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'blobData'); ?>
		<?php echo $form->textField($model,'blobData'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->