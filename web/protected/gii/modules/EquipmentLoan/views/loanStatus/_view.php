<?php
/* @var $this LoanStatusController */
/* @var $data LoanStatus */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('loanstatusid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->loanstatusid), array('view', 'id'=>$data->loanstatusid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>