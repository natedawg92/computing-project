<?php
/* @var $this LoanStatusController */
/* @var $model LoanStatus */

$this->breadcrumbs=array(
	'Loan Statuses'=>array('index'),
	$model->loanstatusid=>array('view','id'=>$model->loanstatusid),
	'Update',
);

$this->menu=array(
	array('label'=>'List LoanStatus', 'url'=>array('index')),
	array('label'=>'Create LoanStatus', 'url'=>array('create')),
	array('label'=>'View LoanStatus', 'url'=>array('view', 'id'=>$model->loanstatusid)),
	array('label'=>'Manage LoanStatus', 'url'=>array('admin')),
);
?>

<h1>Update LoanStatus <?php echo $model->loanstatusid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>