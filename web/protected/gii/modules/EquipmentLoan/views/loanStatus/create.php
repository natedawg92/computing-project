<?php
/* @var $this LoanStatusController */
/* @var $model LoanStatus */

$this->breadcrumbs=array(
	'Loan Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LoanStatus', 'url'=>array('index')),
	array('label'=>'Manage LoanStatus', 'url'=>array('admin')),
);
?>

<h1>Create LoanStatus</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>