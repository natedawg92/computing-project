<?php
/* @var $this LoanStatusController */
/* @var $model LoanStatus */

$this->breadcrumbs=array(
	'Loan Statuses'=>array('index'),
	$model->loanstatusid,
);

$this->menu=array(
	array('label'=>'List LoanStatus', 'url'=>array('index')),
	array('label'=>'Create LoanStatus', 'url'=>array('create')),
	array('label'=>'Update LoanStatus', 'url'=>array('update', 'id'=>$model->loanstatusid)),
	array('label'=>'Delete LoanStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->loanstatusid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LoanStatus', 'url'=>array('admin')),
);
?>

<h1>View LoanStatus #<?php echo $model->loanstatusid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'loanstatusid',
		'status',
	),
)); ?>
