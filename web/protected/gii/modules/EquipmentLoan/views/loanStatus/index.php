<?php
/* @var $this LoanStatusController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Loan Statuses',
);

$this->menu=array(
	array('label'=>'Create LoanStatus', 'url'=>array('create')),
	array('label'=>'Manage LoanStatus', 'url'=>array('admin')),
);
?>

<h1>Loan Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
