<?php
/* @var $this EquipmentLoanRequestController */
/* @var $model EquipmentLoanRequest */

$this->breadcrumbs=array(
	'Equipment Loan Requests'=>array('index'),
	$model->equipmentloanrequestid,
);

$this->menu=array(
	array('label'=>'List EquipmentLoanRequest', 'url'=>array('index')),
	array('label'=>'Create EquipmentLoanRequest', 'url'=>array('create')),
	array('label'=>'Update EquipmentLoanRequest', 'url'=>array('update', 'id'=>$model->equipmentloanrequestid)),
	array('label'=>'Delete EquipmentLoanRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->equipmentloanrequestid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EquipmentLoanRequest', 'url'=>array('admin')),
);
?>

<h1>View EquipmentLoanRequest #<?php echo $model->equipmentloanrequestid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'equipmentloanrequestid',
		'loanStartDat',
		'loanEndDate',
		'userid',
		'loanstatusid',
	),
)); ?>
