<?php
/* @var $this EquipmentLoanRequestController */
/* @var $data EquipmentLoanRequest */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('equipmentloanrequestid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->equipmentloanrequestid), array('view', 'id'=>$data->equipmentloanrequestid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loanStartDat')); ?>:</b>
	<?php echo CHtml::encode($data->loanStartDat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loanEndDate')); ?>:</b>
	<?php echo CHtml::encode($data->loanEndDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loanstatusid')); ?>:</b>
	<?php echo CHtml::encode($data->loanstatusid); ?>
	<br />


</div>