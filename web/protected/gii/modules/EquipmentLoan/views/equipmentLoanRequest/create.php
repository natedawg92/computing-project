<?php
/* @var $this EquipmentLoanRequestController */
/* @var $model EquipmentLoanRequest */

$this->breadcrumbs=array(
	'Equipment Loan Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EquipmentLoanRequest', 'url'=>array('index')),
	array('label'=>'Manage EquipmentLoanRequest', 'url'=>array('admin')),
);
?>

<h1>Create EquipmentLoanRequest</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>