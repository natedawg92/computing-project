<?php
/* @var $this EquipmentLoanRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Equipment Loan Requests',
);

$this->menu=array(
	array('label'=>'Create EquipmentLoanRequest', 'url'=>array('create')),
	array('label'=>'Manage EquipmentLoanRequest', 'url'=>array('admin')),
);
?>

<h1>Equipment Loan Requests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
