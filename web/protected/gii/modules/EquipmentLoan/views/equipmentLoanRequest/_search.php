<?php
/* @var $this EquipmentLoanRequestController */
/* @var $model EquipmentLoanRequest */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'equipmentloanrequestid'); ?>
		<?php echo $form->textField($model,'equipmentloanrequestid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'loanStartDat'); ?>
		<?php echo $form->textField($model,'loanStartDat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'loanEndDate'); ?>
		<?php echo $form->textField($model,'loanEndDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'loanstatusid'); ?>
		<?php echo $form->textField($model,'loanstatusid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->