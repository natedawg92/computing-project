<?php
/* @var $this EquipmentLoanRequestController */
/* @var $model EquipmentLoanRequest */

$this->breadcrumbs=array(
	'Equipment Loan Requests'=>array('index'),
	$model->equipmentloanrequestid=>array('view','id'=>$model->equipmentloanrequestid),
	'Update',
);

$this->menu=array(
	array('label'=>'List EquipmentLoanRequest', 'url'=>array('index')),
	array('label'=>'Create EquipmentLoanRequest', 'url'=>array('create')),
	array('label'=>'View EquipmentLoanRequest', 'url'=>array('view', 'id'=>$model->equipmentloanrequestid)),
	array('label'=>'Manage EquipmentLoanRequest', 'url'=>array('admin')),
);
?>

<h1>Update EquipmentLoanRequest <?php echo $model->equipmentloanrequestid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>