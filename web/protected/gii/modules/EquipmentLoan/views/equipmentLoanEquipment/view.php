<?php
/* @var $this EquipmentLoanEquipmentController */
/* @var $model EquipmentLoanEquipment */

$this->breadcrumbs=array(
	'Equipment Loan Equipments'=>array('index'),
	$model->equipmentloanrequestid,
);

$this->menu=array(
	array('label'=>'List Equipment Loan Equipments', 'url'=>array('index')),
	array('label'=>'Create EquipmentLoanEquipment', 'url'=>array('create')),
	array('label'=>'Update EquipmentLoanEquipment', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete EquipmentLoanEquipment', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'equipmentloanrequestid'=>$model->equipmentloanrequestid'equipmentoptionid'=>$model->equipmentoptionid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Equipment Loan Equipments', 'url'=>array('admin')),
);
?>

<h1>View EquipmentLoanEquipment</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'equipmentloanrequestid',
		'equipmentoptionid',
		'quantity',
	),
)); ?>
