<?php
/* @var $this EquipmentLoanEquipmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Equipment Loan Equipments',
);

$this->menu=array(
	array('label'=>'Create EquipmentLoanEquipment', 'url'=>array('create')),
	array('label'=>'Manage EquipmentLoanEquipment', 'url'=>array('admin')),
);
?>

<h1>Equipment Loan Equipments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
