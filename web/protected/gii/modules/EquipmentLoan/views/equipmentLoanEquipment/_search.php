<?php
/* @var $this EquipmentLoanEquipmentController */
/* @var $model EquipmentLoanEquipment */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'equipmentloanrequestid'); ?>
		<?php echo $form->textField($model,'equipmentloanrequestid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'equipmentoptionid'); ?>
		<?php echo $form->textField($model,'equipmentoptionid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->