<?php
/* @var $this EquipmentLoanEquipmentController */
/* @var $model EquipmentLoanEquipment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'equipment-loan-equipment-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
            <div class="row">
            <?php echo $form->labelEx($model,'equipmentloanrequestid'); ?>
            <?php echo $form->textField($model,'equipmentloanrequestid'); ?>
            <?php echo $form->error($model,'equipmentloanrequestid'); ?>
        </div>
        
            <div class="row">
            <?php echo $form->labelEx($model,'equipmentoptionid'); ?>
            <?php echo $form->textField($model,'equipmentoptionid'); ?>
            <?php echo $form->error($model,'equipmentoptionid'); ?>
        </div>
        
            <div class="row">
            <?php echo $form->labelEx($model,'quantity'); ?>
            <?php echo $form->textField($model,'quantity',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->error($model,'quantity'); ?>
        </div>
        
    	
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form --> 
