<?php
/* @var $this EquipmentLoanEquipmentController */
/* @var $model EquipmentLoanEquipment */

$this->breadcrumbs=array(
		'Equipment Loan Equipments'=>array('index'),
		'Create',
	);

$this->menu=array(
	array('label'=>'List Equipment Loan Equipments', 'url'=>array('index')),
    array('label'=>'Manage EquipmentLoanEquipment', 'url'=>array('admin')),
);
?>

<h1>Create EquipmentLoanEquipment</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>