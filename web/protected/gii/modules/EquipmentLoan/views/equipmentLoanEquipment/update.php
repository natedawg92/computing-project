<?php
/* @var $this EquipmentLoanEquipmentController */
/* @var $model EquipmentLoanEquipment */

$this->breadcrumbs=array(
	'Equipment Loan Equipments'=>array('index'),
	$model->equipmentloanrequestid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List equipmentloanrequestid', 'url'=>array('index')),
	array('label'=>'Create EquipmentLoanEquipment', 'url'=>array('create')),
	array('label'=>'View EquipmentLoanEquipment', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage equipmentloanrequestid', 'url'=>array('admin')),
); 
?>

<h1>Update EquipmentLoanEquipment</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>