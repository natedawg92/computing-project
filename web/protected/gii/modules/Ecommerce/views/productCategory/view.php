<?php
/* @var $this ProductCategoryController */
/* @var $model ProductCategory */

$this->breadcrumbs=array(
	'Product Categories'=>array('index'),
	$model->productid,
);

$this->menu=array(
	array('label'=>'List Product Categories', 'url'=>array('index')),
	array('label'=>'Create ProductCategory', 'url'=>array('create')),
	array('label'=>'Update ProductCategory', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete ProductCategory', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'productid'=>$model->productid'categoryid'=>$model->categoryid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Product Categories', 'url'=>array('admin')),
);
?>

<h1>View ProductCategory</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'productid',
		'categoryid',
	),
)); ?>
