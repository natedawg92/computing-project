<?php
/* @var $this ProductCategoryController */
/* @var $model ProductCategory */

$this->breadcrumbs=array(
	'Product Categories'=>array('index'),
	$model->productid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List productid', 'url'=>array('index')),
	array('label'=>'Create ProductCategory', 'url'=>array('create')),
	array('label'=>'View ProductCategory', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage productid', 'url'=>array('admin')),
); 
?>

<h1>Update ProductCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>