<?php
/* @var $this ProductOptionController */
/* @var $data ProductOption */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('productoptionid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->productoptionid), array('view', 'id'=>$data->productoptionid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('size')); ?>:</b>
	<?php echo CHtml::encode($data->size); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('colour')); ?>:</b>
	<?php echo CHtml::encode($data->colour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stockLevel')); ?>:</b>
	<?php echo CHtml::encode($data->stockLevel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unitPrice')); ?>:</b>
	<?php echo CHtml::encode($data->unitPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Product_productid')); ?>:</b>
	<?php echo CHtml::encode($data->Product_productid); ?>
	<br />


</div>