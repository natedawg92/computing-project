<?php
/* @var $this ProductOptionController */
/* @var $model ProductOption */

$this->breadcrumbs=array(
	'Product Options'=>array('index'),
	$model->productoptionid,
);

$this->menu=array(
	array('label'=>'List ProductOption', 'url'=>array('index')),
	array('label'=>'Create ProductOption', 'url'=>array('create')),
	array('label'=>'Update ProductOption', 'url'=>array('update', 'id'=>$model->productoptionid)),
	array('label'=>'Delete ProductOption', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->productoptionid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductOption', 'url'=>array('admin')),
);
?>

<h1>View ProductOption #<?php echo $model->productoptionid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'productoptionid',
		'size',
		'colour',
		'stockLevel',
		'unitPrice',
		'Product_productid',
	),
)); ?>
