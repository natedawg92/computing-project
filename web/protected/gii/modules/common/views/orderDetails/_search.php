<?php
/* @var $this OrderDetailsController */
/* @var $model OrderDetails */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'orderdetailid'); ?>
		<?php echo $form->textField($model,'orderdetailid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ticketid'); ?>
		<?php echo $form->textField($model,'ticketid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'productoptionid'); ?>
		<?php echo $form->textField($model,'productoptionid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->