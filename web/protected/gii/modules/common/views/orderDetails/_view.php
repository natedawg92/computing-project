<?php
/* @var $this OrderDetailsController */
/* @var $data OrderDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderdetailid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->orderdetailid), array('view', 'id'=>$data->orderdetailid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ticketid')); ?>:</b>
	<?php echo CHtml::encode($data->ticketid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('productoptionid')); ?>:</b>
	<?php echo CHtml::encode($data->productoptionid); ?>
	<br />


</div>