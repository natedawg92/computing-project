<?php
/* @var $this OrderDetailsController */
/* @var $model OrderDetails */

$this->breadcrumbs=array(
	'Order Details'=>array('index'),
	$model->orderdetailid=>array('view','id'=>$model->orderdetailid),
	'Update',
);

$this->menu=array(
	array('label'=>'List OrderDetails', 'url'=>array('index')),
	array('label'=>'Create OrderDetails', 'url'=>array('create')),
	array('label'=>'View OrderDetails', 'url'=>array('view', 'id'=>$model->orderdetailid)),
	array('label'=>'Manage OrderDetails', 'url'=>array('admin')),
);
?>

<h1>Update OrderDetails <?php echo $model->orderdetailid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>