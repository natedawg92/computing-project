<?php
/* @var $this PurchaseOrderController */
/* @var $model PurchaseOrder */

$this->breadcrumbs=array(
	'Purchase Orders'=>array('index'),
	$model->purchaseorderid=>array('view','id'=>$model->purchaseorderid),
	'Update',
);

$this->menu=array(
	array('label'=>'List PurchaseOrder', 'url'=>array('index')),
	array('label'=>'Create PurchaseOrder', 'url'=>array('create')),
	array('label'=>'View PurchaseOrder', 'url'=>array('view', 'id'=>$model->purchaseorderid)),
	array('label'=>'Manage PurchaseOrder', 'url'=>array('admin')),
);
?>

<h1>Update PurchaseOrder <?php echo $model->purchaseorderid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>