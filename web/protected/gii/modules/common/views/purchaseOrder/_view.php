<?php
/* @var $this PurchaseOrderController */
/* @var $data PurchaseOrder */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('purchaseorderid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->purchaseorderid), array('view', 'id'=>$data->purchaseorderid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dispatched')); ?>:</b>
	<?php echo CHtml::encode($data->dispatched); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentRecieevd')); ?>:</b>
	<?php echo CHtml::encode($data->paymentRecieevd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderDate')); ?>:</b>
	<?php echo CHtml::encode($data->orderDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trackingNum')); ?>:</b>
	<?php echo CHtml::encode($data->trackingNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OrderDetails_orderdetailid')); ?>:</b>
	<?php echo CHtml::encode($data->OrderDetails_orderdetailid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('User_userid')); ?>:</b>
	<?php echo CHtml::encode($data->User_userid); ?>
	<br />


</div>