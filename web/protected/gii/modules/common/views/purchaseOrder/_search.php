<?php
/* @var $this PurchaseOrderController */
/* @var $model PurchaseOrder */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'purchaseorderid'); ?>
		<?php echo $form->textField($model,'purchaseorderid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dispatched'); ?>
		<?php echo $form->textField($model,'dispatched'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paymentRecieevd'); ?>
		<?php echo $form->textField($model,'paymentRecieevd'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orderDate'); ?>
		<?php echo $form->textField($model,'orderDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trackingNum'); ?>
		<?php echo $form->textField($model,'trackingNum',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'OrderDetails_orderdetailid'); ?>
		<?php echo $form->textField($model,'OrderDetails_orderdetailid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'User_userid'); ?>
		<?php echo $form->textField($model,'User_userid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->