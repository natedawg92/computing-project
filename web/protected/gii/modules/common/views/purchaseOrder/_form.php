<?php
/* @var $this PurchaseOrderController */
/* @var $model PurchaseOrder */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'purchase-order-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'dispatched'); ?>
		<?php echo $form->textField($model,'dispatched'); ?>
		<?php echo $form->error($model,'dispatched'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paymentRecieevd'); ?>
		<?php echo $form->textField($model,'paymentRecieevd'); ?>
		<?php echo $form->error($model,'paymentRecieevd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orderDate'); ?>
		<?php echo $form->textField($model,'orderDate'); ?>
		<?php echo $form->error($model,'orderDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trackingNum'); ?>
		<?php echo $form->textField($model,'trackingNum',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'trackingNum'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'OrderDetails_orderdetailid'); ?>
		<?php echo $form->textField($model,'OrderDetails_orderdetailid'); ?>
		<?php echo $form->error($model,'OrderDetails_orderdetailid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'User_userid'); ?>
		<?php echo $form->textField($model,'User_userid'); ?>
		<?php echo $form->error($model,'User_userid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->