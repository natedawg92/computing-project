<?php
/* @var $this MediaCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Media Categories',
);

$this->menu=array(
	array('label'=>'Create MediaCategory', 'url'=>array('create')),
	array('label'=>'Manage MediaCategory', 'url'=>array('admin')),
);
?>

<h1>Media Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
