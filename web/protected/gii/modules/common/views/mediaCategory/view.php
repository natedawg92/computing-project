<?php
/* @var $this MediaCategoryController */
/* @var $model MediaCategory */

$this->breadcrumbs=array(
	'Media Categories'=>array('index'),
	$model->mediaid,
);

$this->menu=array(
	array('label'=>'List Media Categories', 'url'=>array('index')),
	array('label'=>'Create MediaCategory', 'url'=>array('create')),
	array('label'=>'Update MediaCategory', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete MediaCategory', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'mediaid'=>$model->mediaid'categoryid'=>$model->categoryid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Media Categories', 'url'=>array('admin')),
);
?>

<h1>View MediaCategory</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'mediaid',
		'categoryid',
	),
)); ?>
