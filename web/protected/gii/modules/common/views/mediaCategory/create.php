<?php
/* @var $this MediaCategoryController */
/* @var $model MediaCategory */

$this->breadcrumbs=array(
		'Media Categories'=>array('index'),
		'Create',
	);

$this->menu=array(
	array('label'=>'List Media Categories', 'url'=>array('index')),
    array('label'=>'Manage MediaCategory', 'url'=>array('admin')),
);
?>

<h1>Create MediaCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>