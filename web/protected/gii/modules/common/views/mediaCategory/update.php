<?php
/* @var $this MediaCategoryController */
/* @var $model MediaCategory */

$this->breadcrumbs=array(
	'Media Categories'=>array('index'),
	$model->mediaid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List mediaid', 'url'=>array('index')),
	array('label'=>'Create MediaCategory', 'url'=>array('create')),
	array('label'=>'View MediaCategory', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage mediaid', 'url'=>array('admin')),
); 
?>

<h1>Update MediaCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>