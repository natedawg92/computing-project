<?php
/* @var $this EquipmentController */
/* @var $data Equipment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('equipmentid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->equipmentid), array('view', 'id'=>$data->equipmentid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner')); ?>:</b>
	<?php echo CHtml::encode($data->owner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Image')); ?>:</b>
	<?php echo CHtml::encode($data->Image); ?>
	<br />


</div>