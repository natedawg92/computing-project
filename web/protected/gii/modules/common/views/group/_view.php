<?php
/* @var $this GroupController */
/* @var $data Group */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('groupid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->groupid), array('view', 'id'=>$data->groupid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('groupName')); ?>:</b>
	<?php echo CHtml::encode($data->groupName); ?>
	<br />


</div>