<?php
/* @var $this EquipmentOptionController */
/* @var $model EquipmentOption */

$this->breadcrumbs=array(
	'Equipment Options'=>array('index'),
	$model->equipmentoptionid,
);

$this->menu=array(
	array('label'=>'List EquipmentOption', 'url'=>array('index')),
	array('label'=>'Create EquipmentOption', 'url'=>array('create')),
	array('label'=>'Update EquipmentOption', 'url'=>array('update', 'id'=>$model->equipmentoptionid)),
	array('label'=>'Delete EquipmentOption', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->equipmentoptionid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EquipmentOption', 'url'=>array('admin')),
);
?>

<h1>View EquipmentOption #<?php echo $model->equipmentoptionid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'equipmentoptionid',
		'size',
		'colour',
		'quantity',
		'equipmentid',
	),
)); ?>
