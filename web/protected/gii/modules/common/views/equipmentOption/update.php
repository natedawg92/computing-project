<?php
/* @var $this EquipmentOptionController */
/* @var $model EquipmentOption */

$this->breadcrumbs=array(
	'Equipment Options'=>array('index'),
	$model->equipmentoptionid=>array('view','id'=>$model->equipmentoptionid),
	'Update',
);

$this->menu=array(
	array('label'=>'List EquipmentOption', 'url'=>array('index')),
	array('label'=>'Create EquipmentOption', 'url'=>array('create')),
	array('label'=>'View EquipmentOption', 'url'=>array('view', 'id'=>$model->equipmentoptionid)),
	array('label'=>'Manage EquipmentOption', 'url'=>array('admin')),
);
?>

<h1>Update EquipmentOption <?php echo $model->equipmentoptionid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>