<?php
/* @var $this EquipmentOptionController */
/* @var $model EquipmentOption */

$this->breadcrumbs=array(
	'Equipment Options'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EquipmentOption', 'url'=>array('index')),
	array('label'=>'Manage EquipmentOption', 'url'=>array('admin')),
);
?>

<h1>Create EquipmentOption</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>