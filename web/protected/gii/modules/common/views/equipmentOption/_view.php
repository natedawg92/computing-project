<?php
/* @var $this EquipmentOptionController */
/* @var $data EquipmentOption */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('equipmentoptionid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->equipmentoptionid), array('view', 'id'=>$data->equipmentoptionid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('size')); ?>:</b>
	<?php echo CHtml::encode($data->size); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('colour')); ?>:</b>
	<?php echo CHtml::encode($data->colour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('equipmentid')); ?>:</b>
	<?php echo CHtml::encode($data->equipmentid); ?>
	<br />


</div>