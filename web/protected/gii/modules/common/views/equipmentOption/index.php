<?php
/* @var $this EquipmentOptionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Equipment Options',
);

$this->menu=array(
	array('label'=>'Create EquipmentOption', 'url'=>array('create')),
	array('label'=>'Manage EquipmentOption', 'url'=>array('admin')),
);
?>

<h1>Equipment Options</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
