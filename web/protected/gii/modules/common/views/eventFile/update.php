<?php
/* @var $this EventFileController */
/* @var $model EventFile */

$this->breadcrumbs=array(
	'Event Files'=>array('index'),
	$model->eventfileid=>array('view','id'=>$model->eventfileid),
	'Update',
);

$this->menu=array(
	array('label'=>'List EventFile', 'url'=>array('index')),
	array('label'=>'Create EventFile', 'url'=>array('create')),
	array('label'=>'View EventFile', 'url'=>array('view', 'id'=>$model->eventfileid)),
	array('label'=>'Manage EventFile', 'url'=>array('admin')),
);
?>

<h1>Update EventFile <?php echo $model->eventfileid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>