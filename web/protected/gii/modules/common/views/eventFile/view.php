<?php
/* @var $this EventFileController */
/* @var $model EventFile */

$this->breadcrumbs=array(
	'Event Files'=>array('index'),
	$model->eventfileid,
);

$this->menu=array(
	array('label'=>'List EventFile', 'url'=>array('index')),
	array('label'=>'Create EventFile', 'url'=>array('create')),
	array('label'=>'Update EventFile', 'url'=>array('update', 'id'=>$model->eventfileid)),
	array('label'=>'Delete EventFile', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->eventfileid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EventFile', 'url'=>array('admin')),
);
?>

<h1>View EventFile #<?php echo $model->eventfileid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'eventfileid',
		'eventId',
	),
)); ?>
