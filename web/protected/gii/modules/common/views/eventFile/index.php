<?php
/* @var $this EventFileController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Event Files',
);

$this->menu=array(
	array('label'=>'Create EventFile', 'url'=>array('create')),
	array('label'=>'Manage EventFile', 'url'=>array('admin')),
);
?>

<h1>Event Files</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
