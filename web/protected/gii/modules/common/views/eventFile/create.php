<?php
/* @var $this EventFileController */
/* @var $model EventFile */

$this->breadcrumbs=array(
	'Event Files'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EventFile', 'url'=>array('index')),
	array('label'=>'Manage EventFile', 'url'=>array('admin')),
);
?>

<h1>Create EventFile</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>