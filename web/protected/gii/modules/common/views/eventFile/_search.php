<?php
/* @var $this EventFileController */
/* @var $model EventFile */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'eventfileid'); ?>
		<?php echo $form->textField($model,'eventfileid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eventId'); ?>
		<?php echo $form->textField($model,'eventId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->