<?php
/* @var $this EventFileController */
/* @var $data EventFile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('eventfileid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->eventfileid), array('view', 'id'=>$data->eventfileid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eventId')); ?>:</b>
	<?php echo CHtml::encode($data->eventId); ?>
	<br />


</div>