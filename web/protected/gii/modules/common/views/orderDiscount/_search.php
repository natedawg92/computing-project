<?php
/* @var $this OrderDiscountController */
/* @var $model OrderDiscount */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'orderdetailid'); ?>
		<?php echo $form->textField($model,'orderdetailid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'discountid'); ?>
		<?php echo $form->textField($model,'discountid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->