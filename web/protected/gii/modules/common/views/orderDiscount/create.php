<?php
/* @var $this OrderDiscountController */
/* @var $model OrderDiscount */

$this->breadcrumbs=array(
		'Order Discounts'=>array('index'),
		'Create',
	);

$this->menu=array(
	array('label'=>'List Order Discounts', 'url'=>array('index')),
    array('label'=>'Manage OrderDiscount', 'url'=>array('admin')),
);
?>

<h1>Create OrderDiscount</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>