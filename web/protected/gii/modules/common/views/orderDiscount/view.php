<?php
/* @var $this OrderDiscountController */
/* @var $model OrderDiscount */

$this->breadcrumbs=array(
	'Order Discounts'=>array('index'),
	$model->orderdetailid,
);

$this->menu=array(
	array('label'=>'List Order Discounts', 'url'=>array('index')),
	array('label'=>'Create OrderDiscount', 'url'=>array('create')),
	array('label'=>'Update OrderDiscount', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete OrderDiscount', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'orderdetailid'=>$model->orderdetailid'discountid'=>$model->discountid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Order Discounts', 'url'=>array('admin')),
);
?>

<h1>View OrderDiscount</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'orderdetailid',
		'discountid',
	),
)); ?>
