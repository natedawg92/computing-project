<?php
/* @var $this OrderDiscountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Order Discounts',
);

$this->menu=array(
	array('label'=>'Create OrderDiscount', 'url'=>array('create')),
	array('label'=>'Manage OrderDiscount', 'url'=>array('admin')),
);
?>

<h1>Order Discounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
