<?php
/* @var $this OrderDiscountController */
/* @var $model OrderDiscount */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'order-discount-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
            <div class="row">
            <?php echo $form->labelEx($model,'orderdetailid'); ?>
            <?php echo $form->textField($model,'orderdetailid'); ?>
            <?php echo $form->error($model,'orderdetailid'); ?>
        </div>
        
            <div class="row">
            <?php echo $form->labelEx($model,'discountid'); ?>
            <?php echo $form->textField($model,'discountid'); ?>
            <?php echo $form->error($model,'discountid'); ?>
        </div>
        
    	
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form --> 
