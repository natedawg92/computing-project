<?php
/* @var $this OrderDiscountController */
/* @var $model OrderDiscount */

$this->breadcrumbs=array(
	'Order Discounts'=>array('index'),
	$model->orderdetailid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List orderdetailid', 'url'=>array('index')),
	array('label'=>'Create OrderDiscount', 'url'=>array('create')),
	array('label'=>'View OrderDiscount', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage orderdetailid', 'url'=>array('admin')),
); 
?>

<h1>Update OrderDiscount</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>