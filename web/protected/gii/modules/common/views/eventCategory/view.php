<?php
/* @var $this EventCategoryController */
/* @var $model EventCategory */

$this->breadcrumbs=array(
	'Event Categories'=>array('index'),
	$model->eventid,
);

$this->menu=array(
	array('label'=>'List Event Categories', 'url'=>array('index')),
	array('label'=>'Create EventCategory', 'url'=>array('create')),
	array('label'=>'Update EventCategory', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete EventCategory', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'eventid'=>$model->eventid'categoryid'=>$model->categoryid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Event Categories', 'url'=>array('admin')),
);
?>

<h1>View EventCategory</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'eventid',
		'categoryid',
	),
)); ?>
