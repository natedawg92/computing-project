<?php
/* @var $this EventCategoryController */
/* @var $model EventCategory */

$this->breadcrumbs=array(
	'Event Categories'=>array('index'),
	$model->eventid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List eventid', 'url'=>array('index')),
	array('label'=>'Create EventCategory', 'url'=>array('create')),
	array('label'=>'View EventCategory', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage eventid', 'url'=>array('admin')),
); 
?>

<h1>Update EventCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>