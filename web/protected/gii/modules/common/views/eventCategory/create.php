<?php
/* @var $this EventCategoryController */
/* @var $model EventCategory */

$this->breadcrumbs=array(
		'Event Categories'=>array('index'),
		'Create',
	);

$this->menu=array(
	array('label'=>'List Event Categories', 'url'=>array('index')),
    array('label'=>'Manage EventCategory', 'url'=>array('admin')),
);
?>

<h1>Create EventCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>