<?php
/* @var $this DiscountController */
/* @var $model Discount */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'discount-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'discountCode'); ?>
		<?php echo $form->textField($model,'discountCode',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'discountCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bizrule'); ?>
		<?php echo $form->textArea($model,'bizrule',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'bizrule'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'discountAmount'); ?>
		<?php echo $form->textField($model,'discountAmount',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'discountAmount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'discountPercent'); ?>
		<?php echo $form->textField($model,'discountPercent'); ?>
		<?php echo $form->error($model,'discountPercent'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->