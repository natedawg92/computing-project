<?php
/* @var $this DiscountController */
/* @var $model Discount */

$this->breadcrumbs=array(
	'Discounts'=>array('index'),
	$model->discountid,
);

$this->menu=array(
	array('label'=>'List Discount', 'url'=>array('index')),
	array('label'=>'Create Discount', 'url'=>array('create')),
	array('label'=>'Update Discount', 'url'=>array('update', 'id'=>$model->discountid)),
	array('label'=>'Delete Discount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->discountid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Discount', 'url'=>array('admin')),
);
?>

<h1>View Discount #<?php echo $model->discountid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'discountid',
		'discountCode',
		'bizrule',
		'discountAmount',
		'discountPercent',
	),
)); ?>
