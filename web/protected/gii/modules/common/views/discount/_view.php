<?php
/* @var $this DiscountController */
/* @var $data Discount */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('discountid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->discountid), array('view', 'id'=>$data->discountid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discountCode')); ?>:</b>
	<?php echo CHtml::encode($data->discountCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bizrule')); ?>:</b>
	<?php echo CHtml::encode($data->bizrule); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discountAmount')); ?>:</b>
	<?php echo CHtml::encode($data->discountAmount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discountPercent')); ?>:</b>
	<?php echo CHtml::encode($data->discountPercent); ?>
	<br />


</div>