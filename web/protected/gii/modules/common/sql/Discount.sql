SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Discount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Discount` (
  `discountid` INT NOT NULL AUTO_INCREMENT COMMENT 'Discount ID',
  `discountCode` VARCHAR(64) NOT NULL COMMENT 'Discount Code',
  `bizrule` TEXT NULL COMMENT 'Business Rule',
  `discountAmount` DECIMAL(5,2) NULL COMMENT 'Discount Amount',
  `discountPercent` INT NULL COMMENT 'Discount Percentage',
  PRIMARY KEY (`discountid`),
  UNIQUE INDEX `discountCode_UNIQUE` (`discountCode` ASC))
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;