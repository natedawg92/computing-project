SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Category` (
  `categoryid` INT NOT NULL AUTO_INCREMENT COMMENT 'Category ID',
  `name` VARCHAR(64) NOT NULL COMMENT 'Category Name',
  `description` TEXT NULL COMMENT 'Category Description',
  `parentID` INT NOT NULL COMMENT 'Parent Category',
  PRIMARY KEY (`categoryid`),
  UNIQUE INDEX `categoryid_UNIQUE` (`categoryid` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `parentCategory` (`parentID` ASC),
  CONSTRAINT `parentCategory`
    FOREIGN KEY (`parentID`)
    REFERENCES `Category` (`categoryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;