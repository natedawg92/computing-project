SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ticket` (
  `ticketid` INT NOT NULL AUTO_INCREMENT COMMENT 'Ticket ID',
  `name` VARCHAR(64) NOT NULL COMMENT 'Ticket Name',
  `description` TEXT NULL COMMENT 'Ticket Description',
  `cost` DECIMAL(5,2) NOT NULL COMMENT 'Ticket Cost',
  `availability` INT NOT NULL COMMENT 'Ticket Availability',
  `eventid` INT NOT NULL COMMENT 'Event',
  PRIMARY KEY (`ticketid`),
  INDEX `ticketEvent` (`eventid` ASC),
  CONSTRAINT `ticketEvent`
    FOREIGN KEY (`eventid`)
    REFERENCES `Event` (`eventid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;