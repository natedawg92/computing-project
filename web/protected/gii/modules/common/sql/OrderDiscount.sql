SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `OrderDiscount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `OrderDiscount` (
  `orderdetailid` INT NOT NULL COMMENT 'Oder Details',
  `discountid` INT NOT NULL COMMENT 'Discount',
  PRIMARY KEY (`orderdetailid`, `discountid`),
  INDEX `orderDiscountOrderDetails` (`orderdetailid` ASC),
  INDEX `orderDiscountDiscount` (`discountid` ASC),
  CONSTRAINT `orderDiscountOrderDetails`
    FOREIGN KEY (`orderdetailid`)
    REFERENCES `OrderDetails` (`orderdetailid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orderDiscountDiscount`
    FOREIGN KEY (`discountid`)
    REFERENCES `Discount` (`discountid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;