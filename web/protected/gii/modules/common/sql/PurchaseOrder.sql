SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `PurchaseOrder`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PurchaseOrder` (
  `purchaseorderid` INT NOT NULL AUTO_INCREMENT COMMENT 'Purchase Order ID',
  `dispatched` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Has the Order Been Dispatched',
  `paymentRecieevd` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Has Payment Been Recieved',
  `orderDate` DATE NOT NULL COMMENT 'Order Date',
  `trackingNum` VARCHAR(64) NULL COMMENT 'Tracking Number',
  `OrderDetails_orderdetailid` INT NOT NULL,
  `User_userid` INT NOT NULL,
  PRIMARY KEY (`purchaseorderid`),
  INDEX `purchaseOrderOrderDetails` (`OrderDetails_orderdetailid` ASC),
  INDEX `purchaseOrderUser` (`User_userid` ASC),
  CONSTRAINT `purchaseOrderOrderDetails`
    FOREIGN KEY (`OrderDetails_orderdetailid`)
    REFERENCES `OrderDetails` (`orderdetailid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `purchaseOrderUser`
    FOREIGN KEY (`User_userid`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;