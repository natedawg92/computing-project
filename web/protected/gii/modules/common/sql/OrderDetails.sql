SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `OrderDetails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `OrderDetails` (
  `orderdetailid` INT NOT NULL AUTO_INCREMENT COMMENT 'Order Details ID',
  `quantity` INT NULL COMMENT 'Quantity',
  `ticketid` INT NULL,
  `productoptionid` INT NULL COMMENT 'Product Option',
  PRIMARY KEY (`orderdetailid`),
  INDEX `orderDetailsTicket` (`ticketid` ASC),
  INDEX `orderDetailsProductOption` (`productoptionid` ASC),
  CONSTRAINT `orderDetailsTicket`
    FOREIGN KEY (`ticketid`)
    REFERENCES `Ticket` (`ticketid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orderDetailsProductOption`
    FOREIGN KEY (`productoptionid`)
    REFERENCES `ProductOption` (`productoptionid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;