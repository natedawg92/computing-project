SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `MediaCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MediaCategory` (
  `mediaid` INT NOT NULL COMMENT 'Media',
  `categoryid` INT NOT NULL COMMENT 'Category',
  INDEX `mediaCategoryMedia` (`mediaid` ASC),
  INDEX `mediaCategoryCategory` (`categoryid` ASC),
  PRIMARY KEY (`mediaid`, `categoryid`),
  CONSTRAINT `mediaCategoryMedia`
    FOREIGN KEY (`mediaid`)
    REFERENCES `Media` (`mediaid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `mediaCategoryCategory`
    FOREIGN KEY (`categoryid`)
    REFERENCES `Category` (`categoryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;