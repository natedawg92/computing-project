SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Equipment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Equipment` (
  `equipmentid` INT NOT NULL AUTO_INCREMENT COMMENT 'Equipment ID',
  `name` VARCHAR(64) NOT NULL COMMENT 'Equipment Name',
  `description` TEXT NULL COMMENT 'Equipment Description',
  `owner` INT NOT NULL COMMENT 'Owning Group',
  `Image` INT NOT NULL COMMENT 'Equipment Image',
  PRIMARY KEY (`equipmentid`),
  INDEX `equipmentGroup` (`owner` ASC),
  INDEX `equipmentImage` (`Image` ASC),
  CONSTRAINT `equipmentGroup`
    FOREIGN KEY (`owner`)
    REFERENCES `Group` (`groupid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `equipmentImage`
    FOREIGN KEY (`Image`)
    REFERENCES `Media` (`mediaid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;