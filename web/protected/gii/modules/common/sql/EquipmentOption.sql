SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `EquipmentLoanRequest`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EquipmentLoanRequest` (
  `equipmentloanrequestid` INT NOT NULL AUTO_INCREMENT COMMENT 'Equipment Loan Request ID',
  `loanStartDat` DATETIME NOT NULL COMMENT 'Loan Start Date',
  `loanEndDate` DATETIME NOT NULL COMMENT 'Loan End Date',
  `userid` INT NOT NULL COMMENT 'User',
  `loanstatusid` INT NOT NULL COMMENT 'Loan Status',
  PRIMARY KEY (`equipmentloanrequestid`),
  INDEX `equipmentLoanRequestUser` (`userid` ASC),
  INDEX `equipmentLoanRequestLoanStatus` (`loanstatusid` ASC),
  CONSTRAINT `equipmentLoanRequestUser`
    FOREIGN KEY (`userid`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `equipmentLoanRequestLoanStatus`
    FOREIGN KEY (`loanstatusid`)
    REFERENCES `LoanStatus` (`loanstatusid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;