SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Event` (
  `eventid` INT NOT NULL AUTO_INCREMENT COMMENT 'Event ID',
  `name` VARCHAR(64) NOT NULL COMMENT 'Event Name',
  `description` TEXT NOT NULL COMMENT 'Event Description',
  `startTime` TIMESTAMP NOT NULL COMMENT 'EVent Start Time',
  `endTime` TIMESTAMP NOT NULL COMMENT 'Event End Time',
  `contactPerson` INT NOT NULL COMMENT 'Contact Person',
  `locationid` INT NOT NULL COMMENT 'Event Location',
  `imageid` INT NOT NULL COMMENT 'Image ID',
  PRIMARY KEY (`eventid`),
  INDEX `eventContactPerson` (`contactPerson` ASC),
  INDEX `eventLocation` (`locationid` ASC),
  INDEX `eventImage` (`imageid` ASC),
  CONSTRAINT `eventContactPerson`
    FOREIGN KEY (`contactPerson`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `eventLocation`
    FOREIGN KEY (`locationid`)
    REFERENCES `Location` (`locationid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `eventImage`
    FOREIGN KEY (`imageid`)
    REFERENCES `Media` (`mediaid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;