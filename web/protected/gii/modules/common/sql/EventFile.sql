SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `EventFile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EventFiles` (
  `eventfileid` INT NOT NULL AUTO_INCREMENT COMMENT 'Event ID',
  `eventId` INT NOT NULL COMMENT 'Event',
  `eventFileId` INT NOT NULL COMMENT 'Event File',
  PRIMARY KEY (`eventid`),
  INDEX `eventFileEvent` (`eventId` ASC),
  INDEX `eventFileFile` (`eventFileId` ASC),
  CONSTRAINT `eventFileEvent`
    FOREIGN KEY (`eventId`)
    REFERENCES `Event` (`eventid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `eventFileFile`
    FOREIGN KEY (`eventFileId`)
    REFERENCES `Media` (`mediaid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;