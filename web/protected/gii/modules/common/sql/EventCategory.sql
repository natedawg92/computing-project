SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `EventCAtegory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EventCategory` (
  `eventid` INT NOT NULL AUTO_INCREMENT COMMENT 'Event',
  `categoryid` VARCHAR(64) NOT NULL COMMENT 'Category',
  PRIMARY KEY (`eventid`, `categoryid`),
  INDEX `EventCategoryEvent` (`eventid` ASC),
  INDEX `EventCategoryCategory` (`categoryid` ASC),
  CONSTRAINT `EventCategoryEvent`
    FOREIGN KEY (`eventid`)
    REFERENCES `Event` (`eventid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION),
  CONSTRAINT `EventCategoryCategory`
    FOREIGN KEY (`categoryid`)
    REFERENCES `Category` (`categoryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;