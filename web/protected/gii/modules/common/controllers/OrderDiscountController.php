<?php

class OrderDiscountController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView(
		$orderdetailid, $discountid	){		
		$model=$this->loadModel(
			$orderdetailid, $discountid		);
		$this->render('view',array('model'=>$model));
	}
	
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('OrderDiscount');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	

	public function actionCreate()
	{
	    $model=new OrderDiscount;

	    if(isset($_POST['ajax']) && $_POST['ajax']==='order-discount-form')
	    {
	        echo CActiveForm::validate($model);
	        Yii::app()->end();
	    }

	    if(isset($_POST['OrderDiscount']))
	    {
	        $model->attributes=$_POST['OrderDiscount'];
	        if($model->validate())
	        {
				$this->saveModel($model);
				$this->redirect(array('view', 
				'orderdetailid'=>$model->orderdetailid, 'discountid'=>$model->discountid				));
	        }
	    }
	    $this->render('create',array('model'=>$model));
	} 
	
	public function actionDelete(
		$orderdetailid, $discountid	){
		if(Yii::app()->request->isPostRequest)
		{
			try
			{
				// we only allow deletion via POST request
				$this->loadModel(
					$orderdetailid, $discountid				)->delete();
			}
			catch(Exception $e) 
			{
				$this->showError($e);
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionUpdate(
		$orderdetailid, $discountid	){
		$model=$this->loadModel(
			$orderdetailid, $discountid		);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrderDiscount']))
		{
			$model->attributes=$_POST['OrderDiscount'];
			$this->saveModel($model);
			$this->redirect(array('view',
				'orderdetailid'=>$model->orderdetailid, 'discountid'=>$model->discountid	        ));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionAdmin()
	{
		$model=new OrderDiscount('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OrderDiscount']))
			$model->attributes=$_GET['OrderDiscount'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function loadModel(
		$orderdetailid, $discountid	){
		$model=OrderDiscount::model()->findByPk(array(
			'orderdetailid'=>$model->orderdetailid, 'discountid'=>$model->discountid		));
		if($model==null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function saveModel($model)
	{
		try
		{
			$model->save();
		}
		catch(Exception $e)
		{
			$this->showError($e);
		}		
	}

	function showError(Exception $e)
	{
		if($e->getCode()==23000)
			$message = "This operation is not permitted due to an existing foreign key reference.";
		else
			$message = "Invalid operation.";
		throw new CHttpException($e->getCode(), $message);
	}		
}