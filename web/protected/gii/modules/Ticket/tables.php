<?php
	/*
	***********************************************************
	**
	** Tables List
	**
	** Include any Tables required by your module to this list
	** Be careful of Foreign Key Dependencies
	** Tables must be listed in the order to be created
	**
	***********************************************************
	*/
	return array(
		/* 
		** Format = "'TableName',"
		*/
		'Category',
		'Event',
		'EventCategory',
		'EventFile',
		'MediaCategory',
		'Ticket',
		'OrderDetails',
		'Discount',
		'OrderDiscount',
		'PurchaseOrder'
	);
?>