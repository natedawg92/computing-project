<?php
/* @var $this GroupRoleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Group Roles',
);

$this->menu=array(
	array('label'=>'Create GroupRole', 'url'=>array('create')),
	array('label'=>'Manage GroupRole', 'url'=>array('admin')),
);
?>

<h1>Group Roles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
