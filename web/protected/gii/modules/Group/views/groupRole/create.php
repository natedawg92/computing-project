<?php
/* @var $this GroupRoleController */
/* @var $model GroupRole */

$this->breadcrumbs=array(
		'Group Roles'=>array('index'),
		'Create',
	);

$this->menu=array(
	array('label'=>'List Group Roles', 'url'=>array('index')),
    array('label'=>'Manage GroupRole', 'url'=>array('admin')),
);
?>

<h1>Create GroupRole</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>