<?php
/* @var $this GroupRoleController */
/* @var $model GroupRole */

$this->breadcrumbs=array(
	'Group Roles'=>array('index'),
	$model->roleid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List roleid', 'url'=>array('index')),
	array('label'=>'Create GroupRole', 'url'=>array('create')),
	array('label'=>'View GroupRole', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage roleid', 'url'=>array('admin')),
); 
?>

<h1>Update GroupRole</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>