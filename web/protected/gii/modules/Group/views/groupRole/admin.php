<?php
/* @var $this GroupRoleController */
/* @var $model GroupRole */

$this->breadcrumbs=array(
	'Group Roles'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Group Roles', 'url'=>array('index')),
	array('label'=>'Create Group Role', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#group-role-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1>Manage Group Roles</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<a class="search-button" href="#">Advanced Search</a><div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?></div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'group-role-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'roleid',
		'groupid',
		'userid',
    array('class'=>'CButtonColumn',),),)); 
?>

