<?php
/* @var $this GroupRoleController */
/* @var $model GroupRole */

$this->breadcrumbs=array(
	'Group Roles'=>array('index'),
	$model->roleid,
);

$this->menu=array(
	array('label'=>'List Group Roles', 'url'=>array('index')),
	array('label'=>'Create GroupRole', 'url'=>array('create')),
	array('label'=>'Update GroupRole', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete GroupRole', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'roleid'=>$model->roleid,'groupid'=>$model->groupid'userid'=>$model->userid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Group Roles', 'url'=>array('admin')),
);
?>

<h1>View GroupRole</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'roleid',
		'groupid',
		'userid',
	),
)); ?>
