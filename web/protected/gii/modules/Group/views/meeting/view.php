<?php
/* @var $this MeetingController */
/* @var $model Meeting */

$this->breadcrumbs=array(
	'Meetings'=>array('index'),
	$model->meetingid,
);

$this->menu=array(
	array('label'=>'List Meeting', 'url'=>array('index')),
	array('label'=>'Create Meeting', 'url'=>array('create')),
	array('label'=>'Update Meeting', 'url'=>array('update', 'id'=>$model->meetingid)),
	array('label'=>'Delete Meeting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->meetingid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Meeting', 'url'=>array('admin')),
);
?>

<h1>View Meeting #<?php echo $model->meetingid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'meetingid',
		'startTime',
		'endTime',
		'meetingDay',
		'sectionid',
		'groupid',
		'locationid',
	),
)); ?>
