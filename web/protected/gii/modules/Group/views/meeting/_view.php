<?php
/* @var $this MeetingController */
/* @var $data Meeting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('meetingid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->meetingid), array('view', 'id'=>$data->meetingid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startTime')); ?>:</b>
	<?php echo CHtml::encode($data->startTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endTime')); ?>:</b>
	<?php echo CHtml::encode($data->endTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meetingDay')); ?>:</b>
	<?php echo CHtml::encode($data->meetingDay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sectionid')); ?>:</b>
	<?php echo CHtml::encode($data->sectionid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('groupid')); ?>:</b>
	<?php echo CHtml::encode($data->groupid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('locationid')); ?>:</b>
	<?php echo CHtml::encode($data->locationid); ?>
	<br />


</div>