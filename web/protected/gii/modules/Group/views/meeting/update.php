<?php
/* @var $this MeetingController */
/* @var $model Meeting */

$this->breadcrumbs=array(
	'Meetings'=>array('index'),
	$model->meetingid=>array('view','id'=>$model->meetingid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Meeting', 'url'=>array('index')),
	array('label'=>'Create Meeting', 'url'=>array('create')),
	array('label'=>'View Meeting', 'url'=>array('view', 'id'=>$model->meetingid)),
	array('label'=>'Manage Meeting', 'url'=>array('admin')),
);
?>

<h1>Update Meeting <?php echo $model->meetingid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>