<?php
/* @var $this MeetingController */
/* @var $model Meeting */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'meeting-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'startTime'); ?>
		<?php echo $form->textField($model,'startTime'); ?>
		<?php echo $form->error($model,'startTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endTime'); ?>
		<?php echo $form->textField($model,'endTime'); ?>
		<?php echo $form->error($model,'endTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meetingDay'); ?>
		<?php echo $form->textField($model,'meetingDay',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'meetingDay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sectionid'); ?>
		<?php echo $form->textField($model,'sectionid'); ?>
		<?php echo $form->error($model,'sectionid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'groupid'); ?>
		<?php echo $form->textField($model,'groupid'); ?>
		<?php echo $form->error($model,'groupid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'locationid'); ?>
		<?php echo $form->textField($model,'locationid'); ?>
		<?php echo $form->error($model,'locationid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->