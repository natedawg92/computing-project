<?php
/* @var $this SectionController */
/* @var $data Section */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('sectionid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->sectionid), array('view', 'id'=>$data->sectionid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sectionName')); ?>:</b>
	<?php echo CHtml::encode($data->sectionName); ?>
	<br />


</div>