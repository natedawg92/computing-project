<?php
	/*
	***********************************************************
	**
	** Tables List
	**
	** Include any Tables required by your module to this list
	** Be careful of Foreign Key Dependencies
	** Tables must be listed in the order to be created
	**
	***********************************************************
	**
	** Tables For Module 'Group'
	**
	***********************************************************
	*/
	return array(
		/* 
		** Format = "'TableName',"
		*/
		'Category',
		'MediaCategory',
		'Group',
		'Role',
		'GroupRole',
		'Section',
		'Meeting'
	);
?>