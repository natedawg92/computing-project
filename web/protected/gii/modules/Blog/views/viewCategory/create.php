<?php
/* @var $this ViewCategoryController */
/* @var $model ViewCategory */

$this->breadcrumbs=array(
		'View Categories'=>array('index'),
		'Create',
	);

$this->menu=array(
	array('label'=>'List View Categories', 'url'=>array('index')),
    array('label'=>'Manage ViewCategory', 'url'=>array('admin')),
);
?>

<h1>Create ViewCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>