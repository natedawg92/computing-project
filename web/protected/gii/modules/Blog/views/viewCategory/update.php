<?php
/* @var $this ViewCategoryController */
/* @var $model ViewCategory */

$this->breadcrumbs=array(
	'View Categories'=>array('index'),
	$model->viewid=>array('view','id'=>$model->Array),
	'Update',
);

$this->menu=array(
	array('label'=>'List viewid', 'url'=>array('index')),
	array('label'=>'Create ViewCategory', 'url'=>array('create')),
	array('label'=>'View ViewCategory', 'url'=>array('view', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Manage viewid', 'url'=>array('admin')),
); 
?>

<h1>Update ViewCategory</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>