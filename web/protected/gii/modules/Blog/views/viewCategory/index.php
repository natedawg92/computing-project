<?php
/* @var $this ViewCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'View Categories',
);

$this->menu=array(
	array('label'=>'Create ViewCategory', 'url'=>array('create')),
	array('label'=>'Manage ViewCategory', 'url'=>array('admin')),
);
?>

<h1>View Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
