<?php
/* @var $this ViewCategoryController */
/* @var $model ViewCategory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'view-category-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
            <div class="row">
            <?php echo $form->labelEx($model,'viewid'); ?>
            <?php echo $form->textField($model,'viewid'); ?>
            <?php echo $form->error($model,'viewid'); ?>
        </div>
        
            <div class="row">
            <?php echo $form->labelEx($model,'categoryid'); ?>
            <?php echo $form->textField($model,'categoryid'); ?>
            <?php echo $form->error($model,'categoryid'); ?>
        </div>
        
    	
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form --> 
