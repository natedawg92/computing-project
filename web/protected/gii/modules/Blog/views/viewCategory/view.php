<?php
/* @var $this ViewCategoryController */
/* @var $model ViewCategory */

$this->breadcrumbs=array(
	'View Categories'=>array('index'),
	$model->viewid,
);

$this->menu=array(
	array('label'=>'List View Categories', 'url'=>array('index')),
	array('label'=>'Create ViewCategory', 'url'=>array('create')),
	array('label'=>'Update ViewCategory', 'url'=>array('update', 'viewid'=>$model->viewid, 'categoryid'=>$model->categoryid, 'somethingotherid'=>$model->somethingotherid)),
	array('label'=>'Delete ViewCategory', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	      										'viewid'=>$model->viewid'categoryid'=>$model->categoryid,	                                           ),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage View Categories', 'url'=>array('admin')),
);
?>

<h1>View ViewCategory</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'viewid',
		'categoryid',
	),
)); ?>
