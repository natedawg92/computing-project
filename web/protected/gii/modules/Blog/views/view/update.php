<?php
/* @var $this ViewController */
/* @var $model View */

$this->breadcrumbs=array(
	'Views'=>array('index'),
	$model->title=>array('view','id'=>$model->viewid),
	'Update',
);

$this->menu=array(
	array('label'=>'List View', 'url'=>array('index')),
	array('label'=>'Create View', 'url'=>array('create')),
	array('label'=>'View View', 'url'=>array('view', 'id'=>$model->viewid)),
	array('label'=>'Manage View', 'url'=>array('admin')),
);
?>

<h1>Update View <?php echo $model->viewid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>