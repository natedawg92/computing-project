<?php
/* @var $this ViewController */
/* @var $data View */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('viewid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->viewid), array('view', 'id'=>$data->viewid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('authorID')); ?>:</b>
	<?php echo CHtml::encode($data->authorID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('viewTypeID')); ?>:</b>
	<?php echo CHtml::encode($data->viewTypeID); ?>
	<br />


</div>