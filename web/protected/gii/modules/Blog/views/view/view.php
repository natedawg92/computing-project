<?php
/* @var $this ViewController */
/* @var $model View */

$this->breadcrumbs=array(
	'Views'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List View', 'url'=>array('index')),
	array('label'=>'Create View', 'url'=>array('create')),
	array('label'=>'Update View', 'url'=>array('update', 'id'=>$model->viewid)),
	array('label'=>'Delete View', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->viewid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage View', 'url'=>array('admin')),
);
?>

<h1>View View #<?php echo $model->viewid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'viewid',
		'title',
		'content',
		'authorID',
		'viewTypeID',
	),
)); ?>
