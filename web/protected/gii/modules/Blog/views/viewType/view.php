<?php
/* @var $this ViewTypeController */
/* @var $model ViewType */

$this->breadcrumbs=array(
	'View Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ViewType', 'url'=>array('index')),
	array('label'=>'Create ViewType', 'url'=>array('create')),
	array('label'=>'Update ViewType', 'url'=>array('update', 'id'=>$model->viewTypeid)),
	array('label'=>'Delete ViewType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->viewTypeid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ViewType', 'url'=>array('admin')),
);
?>

<h1>View ViewType #<?php echo $model->viewTypeid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'viewTypeid',
		'name',
		'description',
	),
)); ?>
