<?php
/* @var $this ViewTypeController */
/* @var $data ViewType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('viewTypeid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->viewTypeid), array('view', 'id'=>$data->viewTypeid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>