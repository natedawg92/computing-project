<?php
/* @var $this ViewTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'View Types',
);

$this->menu=array(
	array('label'=>'Create ViewType', 'url'=>array('create')),
	array('label'=>'Manage ViewType', 'url'=>array('admin')),
);
?>

<h1>View Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
