<?php
/* @var $this ViewTypeController */
/* @var $model ViewType */

$this->breadcrumbs=array(
	'View Types'=>array('index'),
	$model->name=>array('view','id'=>$model->viewTypeid),
	'Update',
);

$this->menu=array(
	array('label'=>'List ViewType', 'url'=>array('index')),
	array('label'=>'Create ViewType', 'url'=>array('create')),
	array('label'=>'View ViewType', 'url'=>array('view', 'id'=>$model->viewTypeid)),
	array('label'=>'Manage ViewType', 'url'=>array('admin')),
);
?>

<h1>Update ViewType <?php echo $model->viewTypeid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>