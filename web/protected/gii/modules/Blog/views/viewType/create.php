<?php
/* @var $this ViewTypeController */
/* @var $model ViewType */

$this->breadcrumbs=array(
	'View Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ViewType', 'url'=>array('index')),
	array('label'=>'Manage ViewType', 'url'=>array('admin')),
);
?>

<h1>Create ViewType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>