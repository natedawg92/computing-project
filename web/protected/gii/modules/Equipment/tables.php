<?php
	/*
	***********************************************************
	**
	** Tables List
	**
	** Include any Tables required by your module to this list
	** Be careful of Foreign Key Dependencies
	** Tables must be listed in the order to be created
	**
	***********************************************************
	**
	** Tables For Module 'Equipment'
	**
	***********************************************************
	*/
	return array(
		/* 
		** Format = "'TableName',"
		*/
		'Category',
		'MediaCategory',
		'Group',
		'Equipment',
		'EquipmentOption',
	);
?>