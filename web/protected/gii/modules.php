<?php
	/*
	***************************************************************************************
	**
	** Modules List
	**
	** Add any additional Modules to this list to be included in the setup wizard
	** The ModuleName must match the title of the folder where the Module files are kept
	**
	***************************************************************************************
	*/
	return array(
		/*
		** Format = "'ModuleName' => 'Module Title',"
		*/
		'Blog' => 'Blog',
		'Ecommerce' => 'Ecommerce Store',
		'Equipment' => 'Equipment Records',
		'EquipmentLoan' => 'Equipment Loans',
		'Event' => 'Events Calendar',
		'Ticket' => 'Event Ticket Purchasing',
		'Group' => 'Group Records',
	);
?>