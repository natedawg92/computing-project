<?php

/**
 * This is the model class for table "View".
 *
 * The followings are the available columns in table 'View':
 * @property integer $viewid
 * @property string $title
 * @property string $content
 * @property integer $authorID
 * @property integer $viewTypeID
 *
 * The followings are the available model relations:
 * @property User $author
 * @property ViewType $viewType
 * @property Category[] $categories
 */
class View extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'View';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content, authorID, viewTypeID', 'required'),
			array('authorID, viewTypeID', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('viewid, title, content, authorID, viewTypeID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'authorID'),
			'viewType' => array(self::BELONGS_TO, 'ViewType', 'viewTypeID'),
			'categories' => array(self::MANY_MANY, 'Category', 'ViewCategory(viewid, categoryid)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'viewid' => 'View ID',
			'title' => 'View Title',
			'content' => 'View Content',
			'authorID' => 'Author',
			'viewTypeID' => 'View Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('viewid',$this->viewid);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('authorID',$this->authorID);
		$criteria->compare('viewTypeID',$this->viewTypeID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return View the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
