<?php

/**
 * This is the model class for table "EquipmentLoanRequest".
 *
 * The followings are the available columns in table 'EquipmentLoanRequest':
 * @property integer $equipmentloanrequestid
 * @property string $loanStartDat
 * @property string $loanEndDate
 * @property integer $userid
 * @property integer $loanstatusid
 *
 * The followings are the available model relations:
 * @property User $user
 * @property LoanStatus $loanstatus
 */
class EquipmentLoanRequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EquipmentLoanRequest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('loanStartDat, loanEndDate, userid, loanstatusid', 'required'),
			array('userid, loanstatusid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('equipmentloanrequestid, loanStartDat, loanEndDate, userid, loanstatusid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userid'),
			'loanstatus' => array(self::BELONGS_TO, 'LoanStatus', 'loanstatusid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'equipmentloanrequestid' => 'Equipment Loan Request ID',
			'loanStartDat' => 'Loan Start Date',
			'loanEndDate' => 'Loan End Date',
			'userid' => 'User',
			'loanstatusid' => 'Loan Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('equipmentloanrequestid',$this->equipmentloanrequestid);
		$criteria->compare('loanStartDat',$this->loanStartDat,true);
		$criteria->compare('loanEndDate',$this->loanEndDate,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('loanstatusid',$this->loanstatusid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EquipmentLoanRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
