<?php

/**
 * This is the model class for table "Meeting".
 *
 * The followings are the available columns in table 'Meeting':
 * @property integer $meetingid
 * @property string $startTime
 * @property string $endTime
 * @property string $meetingDay
 * @property integer $sectionid
 * @property integer $groupid
 * @property integer $locationid
 *
 * The followings are the available model relations:
 * @property Section $section
 * @property Group $group
 * @property Location $location
 */
class Meeting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Meeting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('startTime, sectionid, groupid, locationid', 'required'),
			array('sectionid, groupid, locationid', 'numerical', 'integerOnly'=>true),
			array('meetingDay', 'length', 'max'=>45),
			array('endTime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('meetingid, startTime, endTime, meetingDay, sectionid, groupid, locationid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'section' => array(self::BELONGS_TO, 'Section', 'sectionid'),
			'group' => array(self::BELONGS_TO, 'Group', 'groupid'),
			'location' => array(self::BELONGS_TO, 'Location', 'locationid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'meetingid' => 'Meetingid',
			'startTime' => 'Meeting Start Time',
			'endTime' => 'Meeting End Time',
			'meetingDay' => 'Meeting Day',
			'sectionid' => 'Sectionid',
			'groupid' => 'Groupid',
			'locationid' => 'Locationid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('meetingid',$this->meetingid);
		$criteria->compare('startTime',$this->startTime,true);
		$criteria->compare('endTime',$this->endTime,true);
		$criteria->compare('meetingDay',$this->meetingDay,true);
		$criteria->compare('sectionid',$this->sectionid);
		$criteria->compare('groupid',$this->groupid);
		$criteria->compare('locationid',$this->locationid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Meeting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
