<?php

/**
 * This is the model class for table "ProductOption".
 *
 * The followings are the available columns in table 'ProductOption':
 * @property integer $productoptionid
 * @property string $size
 * @property string $colour
 * @property integer $stockLevel
 * @property string $unitPrice
 * @property integer $Product_productid
 */
class ProductOption extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ProductOption';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('stockLevel, unitPrice, Product_productid', 'required'),
			array('stockLevel, Product_productid', 'numerical', 'integerOnly'=>true),
			array('size, colour', 'length', 'max'=>64),
			array('unitPrice', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('productoptionid, size, colour, stockLevel, unitPrice, Product_productid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'productoptionid' => 'Product Option ID',
			'size' => 'Product Size',
			'colour' => 'Product Colour',
			'stockLevel' => 'Product Stock Level',
			'unitPrice' => 'Product Unit Price',
			'Product_productid' => 'Product Productid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('productoptionid',$this->productoptionid);
		$criteria->compare('size',$this->size,true);
		$criteria->compare('colour',$this->colour,true);
		$criteria->compare('stockLevel',$this->stockLevel);
		$criteria->compare('unitPrice',$this->unitPrice,true);
		$criteria->compare('Product_productid',$this->Product_productid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
