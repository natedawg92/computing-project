<?php

/**
 * This is the model class for table "Ticket".
 *
 * The followings are the available columns in table 'Ticket':
 * @property integer $ticketid
 * @property string $name
 * @property string $description
 * @property string $cost
 * @property integer $availability
 * @property integer $eventid
 *
 * The followings are the available model relations:
 * @property OrderDetails[] $orderDetails
 * @property Event $event
 */
class Ticket extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Ticket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, cost, availability, eventid', 'required'),
			array('availability, eventid', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>64),
			array('cost', 'length', 'max'=>5),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ticketid, name, description, cost, availability, eventid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orderDetails' => array(self::HAS_MANY, 'OrderDetails', 'ticketid'),
			'event' => array(self::BELONGS_TO, 'Event', 'eventid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ticketid' => 'Ticket ID',
			'name' => 'Ticket Name',
			'description' => 'Ticket Description',
			'cost' => 'Ticket Cost',
			'availability' => 'Ticket Availability',
			'eventid' => 'Event',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ticketid',$this->ticketid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('availability',$this->availability);
		$criteria->compare('eventid',$this->eventid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ticket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
