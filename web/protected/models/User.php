<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $userid
 * @property string $username
 * @property string $firstName
 * @property string $lastName
 * @property string $password
 * @property string $email
 */
class User extends CActiveRecord{
	
	public $confirmPassword;
	public $password1;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'User';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//required fields
			array('username, firstName, lastName, password1, confirmPassword, email', 'required'),
			//fields with length requirements
			array('username, firstName, lastName', 'length', 'max'=>64),
			array('password1, confirmPassword', 'length', 'min'=> 8, 'max'=>30, 'message'=>"Passwords must be at least 8 characters long and a maximum of 30 characters."),
			array('email', 'length', 'max'=>128),
			//Unique Fields
			array('email, username', 'unique'),
			//ensure email is valid
			array('email', 'email'),
			//ensure password match
			array('confirmPassword', 'compare', 'compareAttribute'=>'password1', 'message'=>"Passwords don't match"),
			//Hash Password on save
			array('password', 'hashPassword', 'on'=>'save'),
			//Serch fields
			array('userid, username, firstName, lastName, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'roles'=>array(self::HAS_MANY, 'AuthAssignment', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'username' => 'Username',
			'firstName' => 'First Name',
			'lastName' => 'Surname',
			'password1' => 'Password',
			'confirmPassword' => 'Confirm Password',
			'email' => 'Email Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password,$this->password);
    }
 
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
}
