<?php

class Database extends CFormModel
{

    public $host;
    public $dbname;
    public $username;
	public $password;
	public $confirmPassword;
	
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//required Fields
			array('host, dbname, username, password, confirmPassword', 'required'),
			//ensure passwords match
			array('confirmPassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match"),
			array('host', 'authenticate'),
		);
	}

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
            return array(
					'host'=>'MySQL host',
					'dbName'=>'Database Name',
					'username'=>'Username',
                    'password'=>'password',
                    'confirmPassword'=>'Confirm your password',
            );
    }
	
	public function authenticate($attribute,$params){
		try {
			$dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname;
			$connection = new CDbConnection($dsn,$this->username,$this->password);
			$connection->active=true;
			$connection->active=false;
		} catch (Exception $e) {
			$this->addError('DB_ERROR','Unable to connect to the Database.');
		}
	}
}