<?php

/**
 * This is the model class for table "Location".
 *
 * The followings are the available columns in table 'Location':
 * @property integer $locationid
 * @property string $name
 * @property string $addressLine1
 * @property string $addressLine2
 * @property string $town
 * @property string $postcode
 * @property string $latitude
 * @property string $longitude
 * @property string $link
 * @property integer $mediaid
 */
class Location extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, addressLine1, town, postcode', 'required'),
			array('mediaid', 'numerical', 'integerOnly'=>true),
			array('name, addressLine1, addressLine2, town', 'length', 'max'=>128),
			array('postcode', 'length', 'max'=>8),
			array('latitude', 'length', 'max'=>11),
			array('longitude', 'length', 'max'=>12),
			array('link', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('locationid, name, addressLine1, addressLine2, town, postcode, latitude, longitude, link, mediaid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'locationid' => 'Location ID',
			'name' => 'Location Name',
			'addressLine1' => 'Address Line 1',
			'addressLine2' => 'Address Line 2',
			'town' => 'Town / City',
			'postcode' => 'Postcode',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'link' => 'Website Link',
			'mediaid' => 'Featured Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('locationid',$this->locationid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('addressLine1',$this->addressLine1,true);
		$criteria->compare('addressLine2',$this->addressLine2,true);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('mediaid',$this->mediaid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
