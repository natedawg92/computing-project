<?php

/**
 * This is the model class for table "Event".
 *
 * The followings are the available columns in table 'Event':
 * @property integer $eventid
 * @property string $name
 * @property string $description
 * @property string $startTime
 * @property string $endTime
 * @property integer $contactPerson
 * @property integer $locationid
 * @property integer $imageid
 *
 * The followings are the available model relations:
 * @property User $contactPerson0
 * @property Location $location
 * @property Media $image
 * @property Ticket[] $tickets
 */
class Event extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, startTime, contactPerson, locationid, imageid', 'required'),
			array('contactPerson, locationid, imageid', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>64),
			array('endTime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('eventid, name, description, startTime, endTime, contactPerson, locationid, imageid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contactPerson0' => array(self::BELONGS_TO, 'User', 'contactPerson'),
			'location' => array(self::BELONGS_TO, 'Location', 'locationid'),
			'image' => array(self::BELONGS_TO, 'Media', 'imageid'),
			'tickets' => array(self::HAS_MANY, 'Ticket', 'eventid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'eventid' => 'Event ID',
			'name' => 'Event Name',
			'description' => 'Event Description',
			'startTime' => 'EVent Start Time',
			'endTime' => 'Event End Time',
			'contactPerson' => 'Contact Person',
			'locationid' => 'Event Location',
			'imageid' => 'Image ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('startTime',$this->startTime,true);
		$criteria->compare('endTime',$this->endTime,true);
		$criteria->compare('contactPerson',$this->contactPerson);
		$criteria->compare('locationid',$this->locationid);
		$criteria->compare('imageid',$this->imageid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Event the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
