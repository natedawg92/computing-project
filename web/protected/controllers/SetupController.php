<?php

class SetupController extends Controller{

	public function filters()
	{
		return array(
			array('application.components.yiiBooster.filters.BoosterFilter - delete')
			//array('application.components.FirePHPCore.fb')
		);
	}
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
	public $models = array();
	public $templates = array();
	public $templatePath;
	public $newFileMode=0666;
	public $newDirMode=0755;
	public $tableSchema;

	public function actionIndex(){
		$steps = array('Database', 'User', 'Module');
		$tabs = array();
		array_push($tabs, array('label'=>'Welcome', 'active'=> false, 'content'=> $this->renderPartial('_startPage', array(), true)));
		foreach($steps as $step){
			if(isset($_POST[$step])){
				$function = 'prepare'.$step;
				$this->$function();
			} else {
				switch($step){
					case 'Database':
						try{
							$connectionString = yii::app()->db->connectionString;
						}catch (Exception $e){
							$model = new Database;
							array_push($tabs, array('label'=>$step, 'active'=> false, 'content'=> $this->renderPartial('_databaseForm', array('model'=>$model), true)));
						}
					break;
					case 'User':
						try{
							if(!$this->findAdmin() /*Admin user doesn't exist*/){
								$model = new User;
								array_push($tabs, array('label'=>'Admin User', 'active'=> false, 'content'=> $this->renderPartial('_userForm', array('model'=>$model), true)));
							}
						}catch (Exception $e){
							
						}
					break;
					case 'Module': 
						$model = new Module;
						$data = include YiiBase::getPathOfAlias('application.gii.').'/modules.php';
						array_push($tabs, array('label'=>'Modules', 'active'=> false, 'content'=> $this->renderPartial('_moduleForm', array('model'=>$model, 'data'=>$data), true)));
					break;
				}
			}
		}
		$this->render('index', array('tabs'=>$tabs));
	}

	//Search for a User With 'admin' Privelages
	private function findAdmin(){
		//Get ALL users
		$users = User::model()->findAll();
		//rotate through each user checking if their have the correct privileges
		foreach($users as $u){
			if(yii::app()->user->checkAccess('admin', array('userid'=>$u->userid, false))){
				//if user is 'admin' return true.
				return true;
			}
		}
		//if 'admin' user is not found return false
		return false;
	}

	private function prepareDatabase(){
		$model = new Database;
		if(isset($_POST['Database'])){
			$model->attributes = $_POST['Database'];
			if($model->validate()){
				//set File Path
				$path = YiiBase::getPathOfAlias('application.config');
				//Slect file from path
				$fName = $path.'/main.php';;
				//read file contents
				$content = file_get_contents($fName,null);
				
				//edit file contents
				$content = str_replace("'connectionString' => 'mysql:host=; dbname='", "'connectionString' => 'mysql:host=$model->host; dbname=$model->dbname'", $content);		
				$content = str_replace("'username' => ''", "'username' => '$model->username'",$content);
				$content = str_replace("'password' => ''", "'password' => '$model->password'", $content);

				//Write contents to file
				file_put_contents($fName, $content);

				$this->createTables('Setup');
				$this->createMVC('Setup');
			}
			return true;
		} else {
			return $model;
		}
	}

	private static function searchTables($table){
		$tableNames = Yii::app()->db->schema->tableNames;
		foreach ($tableNames as $a){
			if ($a === $table){
				return true;
			}
		}
		return false;
	}

	private function createTables($module){
		$tables = include YiiBase::getPathOfAlias('application.gii.modules.'.$module).'/tables.php';
		$basePath = YiiBase::getPathOfAlias('application.gii.modules.'.$module.'.sql');
		foreach ($tables as $fileName){
			if(!$this->searchTables($fileName)){
				$file = $basePath.'/'.$fileName.'.sql';
				$sql = file_get_contents($file, null);
				Yii::app()->db->createCommand($sql)->query();
			}
		}
	}

	private function prepareUser(){
		$model = new User;
		if(isset($_POST['User'])){
			$model->attributes = $_POST['User'];
			$model->password = $model->password = CPasswordHelper::hashPassword($model->password1);
			if($model->validate()){
				$model->save();
			}
		}
		return $model;
	}

	private function createMVC($module){
		//Get a list of the modles and crud files that need to be generated
		$tables = include YiiBase::getPathOfAlias('application.gii.modules.'.$module).'/tables.php';
		//rotate through the list generating the necesary files.
		foreach ($tables as $className){
			$this->createModel($className, $module);
			$this->createCrud($className, $module);
		}
	}

	private function createModel($className, $module){
		//Check if model already exists
		$filepath = YiiBase::getPathOfAlias('application.models');
		$modulePath = yiiBase::getPathOfAlias('application.gii.modules.'.$module.'.models');
		if(file_exists($modulePath.'/'.$className.'.php')){
			copy(
				$modulePath.'/'.$className.'.php', //Original File
				$filePath.'/'.$className.'.php' //New File
			);
		} elseif(!file_exists($filepath.'/'.$className.'.php')){
			//Model doesnt exist so create it
			//Set template path and load instance of ModelCode
			$this->templatePath = yiibase::getPathOfAlias('application.gii.giiModule.templates.default.model');
			$model = yii::import('gii.generators.model.ModelCode');
			$model = new $model;

			//Set ModelCode Variables
			$model->baseClass = 'CActiveRecord';
			$model->buildRelations = true;
			$model->commentsAsLabels = true;
			$model->connectionId = 'db';
			$model->modelClass = $className;
			$model->modelPath = 'application.models';
			$model->tableName = $className;
			$model->tablePrefix = '';
			$model->template = 'default';
			$this->templates['default'] = $this->templatePath;
			
			//Preape and save Modelcode with the given variables.
			$model->prepare();
			$model->save();
		}
	}

	private function createCRUD($className, $module){
		//Check if Controller has already been created
		$filepath = YiiBase::getPathOfAlias('application.controllers');
		$modulePath = yiiBase::getPathOfAlias('application.gii.modules.'.$module.'.controllers');
		if(!file_exists($filepath.'/'.$className.'Controller.php')){
			//Controller doesnt already exist so check if Crud should be 
			//created with developer given files or Gii Generation
			if(!DEFAULT_CRUD && file_exists($modulePath.'/'.$className.'Controller.php')){
				$this->copyCRUD($className, $module);
			} else {
				$this->defaultCRUD($className);
			}
		}
	}

	private function defaultCRUD($className){
		//Create the Defualt CRUD from Gii Generation. 
		//Set Template Path and Load instance of CrudCode
		$table = CActiveRecord::model($className)->tableSchema;
		if($table->primaryKey === null){
			$this->addError('The Selected Table does not contain a Primary Key');
		} else {
			if(is_array($table->primaryKey)){
				$this->templatePath = yiibase::getPathOfAlias('application.gii.giiModule.templates.composite.crud');
			} else {
				$this->templatePath = yiibase::getPathOfAlias('application.gii.giiModule.templates.default.crud');
			}
			$model = yii::import('gii.generators.crud.CrudCode');
			$model = new $model;

			//Set Model Variables
			$model->baseControllerClass = 'Controller';
			$model->controller = $className;
			$model->model = $className;
			$model->template = 'default';
			$this->templates['default'] = $this->templatePath;
			
			//Preapre and save Model
			$model->validateModel('','');
			$model->prepare();
			$model->save();
		}
	}

	private function copyCRUD($className, $module){
		//Create CRUD from Developer given files.
		$basePath = YiiBase::getPathOfAlias('application.gii.modules.'.$module);
		$viewPath = YiiBase::getPathOfAlias('application.views.'.$className);
		$controllerPath = YiiBase::getPathOfAlias('application.controllers');

		//Copy Controller file from Modules folder to new direcotry
		copy(
			$basePath.'/controllers/'.$className.'Controller.php', //Original File
			$controllerPath.'/'.$className.'Controller.php' //New File
		);

		//Create the Views Directory
		if(!is_dir($viewPath)){
			mkdir($viewPath, $this->newFileMode, true);
		}

		//copy the View Files to the new directory
		$files = array_diff(scandir($basePath.'/views/'.$className), array('..', '.'));
		foreach ($files as $file){
			copy(
				$basePath.'/views/'.$className.'/'.$file, //Original File
				$viewPath.'/'.$file //New File
			);
		}
	}

	private function actionCRUD($className){
		$model = yii::import('gii.generators.crud.CrudCode');
		$model = new $model;

		//Set Model Variables
		$model->baseControllerClass = 'Controller';
		$model->controller = $className;
		$model->model = $className;
		$model->template = 'default';
		$this->templates['default'] = $this->templatePath;

		//Preapre and save Model
		$model->validateModel('','');
		$model->prepare();
		$model->save();
	}

	private function prepareModule(){
		$model = new Module;
		if(isset($_POST['Module'])){
			foreach($_POST['Module']['modules'] as $module){
				$this->createTables($module);
				$this->createMVC($module);
			}
			return true;
		} else {
			return $model;
		}
	}
}