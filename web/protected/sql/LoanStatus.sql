SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `LoanStatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LoanStatus` (
  `loanstatusid` INT NOT NULL AUTO_INCREMENT COMMENT 'Loan Satus ID',
  `status` VARCHAR(45) NULL COMMENT 'Loan Status',
  PRIMARY KEY (`loanstatusid`),
  UNIQUE INDEX `status_UNIQUE` (`status` ASC))
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for Table `LoanStatus`
-- -----------------------------------------------------
INSERT INTO `ComputingProjectMk2`.`LoanStatus` (`loanstatusid`, `status`) VALUES (1, 'Pending');
INSERT INTO `ComputingProjectMk2`.`LoanStatus` (`loanstatusid`, `status`) VALUES (2, 'Granted');
INSERT INTO `ComputingProjectMk2`.`LoanStatus` (`loanstatusid`, `status`) VALUES (3, 'Denied');