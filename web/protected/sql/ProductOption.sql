SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `ProductOption`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ProductOption` (
  `productoptionid` INT NOT NULL AUTO_INCREMENT COMMENT 'Product Option ID',
  `size` VARCHAR(64) NULL COMMENT 'Product Size',
  `colour` VARCHAR(64) NULL COMMENT 'Product Colour',
  `stockLevel` INT NOT NULL COMMENT 'Product Stock Level',
  `unitPrice` DECIMAL(5,2) NOT NULL COMMENT 'Product Unit Price',
  `Product_productid` INT NOT NULL,
  PRIMARY KEY (`productoptionid`),
  INDEX `productOptionProduct` (`Product_productid` ASC),
  CONSTRAINT `productOptionProduct`
    FOREIGN KEY (`Product_productid`)
    REFERENCES `Product` (`productid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;