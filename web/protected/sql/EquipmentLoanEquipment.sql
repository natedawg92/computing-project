SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `EquipmentLoanEquipment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EquipmentLoanEquipment` (
  `equipmentloanrequestid` INT NOT NULL COMMENT 'Equipment Loan',
  `equipmentoptionid` INT NOT NULL COMMENT 'Equipment Option',
  `quantity` VARCHAR(45) NOT NULL COMMENT 'Quantity Requested',
  INDEX `equipmentLoanEquipmentEquipmentLoanRequest` (`equipmentloanrequestid` ASC),
  INDEX `equipmentLoanEquipmentEquipmentOption` (`equipmentoptionid` ASC),
  CONSTRAINT `equipmentLoanEquipmentEquipmentLoanRequest`
    FOREIGN KEY (`equipmentloanrequestid`)
    REFERENCES `EquipmentLoanRequest` (`equipmentloanrequestid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `equipmentLoanEquipmentEquipmentOption`
    FOREIGN KEY (`equipmentoptionid`)
    REFERENCES `EquipmentOption` (`equipmentoptionid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;