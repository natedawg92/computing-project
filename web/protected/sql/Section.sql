SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Section`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Section` (
  `sectionid` INT NOT NULL AUTO_INCREMENT COMMENT 'Section ID',
  `sectionName` VARCHAR(45) NOT NULL COMMENT 'Section Name',
  PRIMARY KEY (`sectionid`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for Table `Section`
-- -----------------------------------------------------
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (1, 'Beavers');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (2, 'Cub Scouts');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (3, 'Scouts');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (4, 'Air Scouts');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (5, 'Sea Scouts');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (6 , 'Explorer Scouts');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (7, 'Air Explorers');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (8, 'Sea Explorers');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (9, 'Young Leaders');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (10, 'Scout Network');
INSERT INTO `ComputingProjectMk2`.`Section` (`sectionid`, `sectionName`) VALUES (11, 'Scout Active Support');