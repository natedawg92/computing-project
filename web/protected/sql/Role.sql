SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Role` (
  `roleid` INT NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `name` VARCHAR(45) NOT NULL COMMENT 'Role Name',
  `description` VARCHAR(45) NOT NULL COMMENT 'Role Description',
  PRIMARY KEY (`roleid`))
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for Table `Role`
-- -----------------------------------------------------
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (1, 'BSL', 'Beaver Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (2, 'ABSL', 'Assistant Beaver Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (3, 'CA', 'Colony Assistant (Beavers Scouts)');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (4, 'CSL', 'Cub Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (5, 'ACSL', 'Assistant Cub Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (6, 'PA', 'Pack Assistant (Cub Scouts)');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (7, 'SL', 'Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (8, 'ASL', 'Assistant Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (9, 'TA', 'Troop Assistant (Scouts)');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (10, 'ESL', 'Explorer Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (11, 'AESL', 'Assistant Explorer Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (12, 'GSL', 'Group Scout Leader');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (13, 'GS', 'Group Secretary');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (14, 'GT', 'Group Treasurer');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (15, 'GC', 'Group Chairman');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (16, 'DC', 'District Commisioner');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (17, 'ADCB', 'Assistant Distrcit Commisioner (Beaver Scouts)');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (18, 'ADCC', 'Assistant District Commisoner (Cub Scouts)');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (19, 'ADCS', 'Assistant District Commisioner (Scouts)');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (20, 'BA', 'Band Advisor');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (21, 'DCh', 'Chairman');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (22, 'MDM', 'Media Development Manager');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (23, 'DS', 'District Secretary');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (24, 'DT', 'District Treasurer');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (25, 'NAA', 'Nights Away Advisor');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (26, 'DDOEAA', 'District Duke of Edingburgh Award Advisor');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (27, 'LTA', 'Local Training Administrator');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (28, 'LTM', 'Local Training Manager');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (29, 'TA', 'Training Advisor');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (30, 'BS', 'Beaver Scout');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (31, 'CS', 'Cub Scout');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (32, 'S', 'Scout');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (33, 'ES', 'Explorer Scout');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (34, 'GQM', 'Group Quarter Master');
INSERT INTO `Role` (`roleid`, `name`, `description`) VALUES (35, 'DQM', 'District Quarter Master');