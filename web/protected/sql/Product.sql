SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Product` (
  `productid` INT NOT NULL AUTO_INCREMENT COMMENT 'Product ID',
  `name` VARCHAR(128) NULL COMMENT 'Product Name',
  `description` TEXT NULL COMMENT 'Product Description',
  `imageid` INT NOT NULL COMMENT 'Image ID',
  PRIMARY KEY (`productid`),
  INDEX `productImage` (`imageid` ASC),
  CONSTRAINT `productImage`
    FOREIGN KEY (`imageid`)
    REFERENCES `Media` (`mediaid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;