SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Meeting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Meeting` (
  `meetingid` INT NOT NULL AUTO_INCREMENT,
  `startTime` TIMESTAMP NOT NULL COMMENT 'Meeting Start Time',
  `endTime` TIMESTAMP NOT NULL COMMENT 'Meeting End Time',
  `meetingDay` VARCHAR(45) NULL COMMENT 'Meeting Day',
  `sectionid` INT NOT NULL,
  `groupid` INT NOT NULL,
  `locationid` INT NOT NULL,
  PRIMARY KEY (`meetingid`),
  INDEX `meetingSection` (`sectionid` ASC),
  INDEX `meetingGroup` (`groupid` ASC),
  INDEX `meetingLocation` (`locationid` ASC),
  CONSTRAINT `meetingSection`
    FOREIGN KEY (`sectionid`)
    REFERENCES `Section` (`sectionid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `meetingGroup`
    FOREIGN KEY (`groupid`)
    REFERENCES `Group` (`groupid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `meetingLocation`
    FOREIGN KEY (`locationid`)
    REFERENCES `Location` (`locationid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;