SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `GroupRole`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GroupRole` (
  `roleid` INT NOT NULL COMMENT 'Role',
  `groupid` INT NOT NULL COMMENT 'Group',
  `userid` INT NOT NULL COMMENT 'User',
  INDEX `groupRoleRole` (`roleid` ASC),
  INDEX `groupRoleGroup` (`groupid` ASC),
  INDEX `groupRoleUser` (`userid` ASC),
  PRIMARY KEY (`roleid`, `groupid`, `userid`),
  CONSTRAINT `groupRoleRole`
    FOREIGN KEY (`roleid`)
    REFERENCES `Role` (`roleid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `groupRoleGroup`
    FOREIGN KEY (`groupid`)
    REFERENCES `Group` (`groupid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `groupRoleUser`
    FOREIGN KEY (`userid`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;