SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `ProductCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ProductCategory` (
  `productid` INT NOT NULL COMMENT 'Product',
  `categoryid` INT NOT NULL COMMENT 'Category',
  INDEX `productCategoryProduct` (`productid` ASC),
  INDEX `productCategoryCategory` (`categoryid` ASC),
  PRIMARY KEY (`productid`, `categoryid`),
  CONSTRAINT `productCategoryProduct`
    FOREIGN KEY (`productid`)
    REFERENCES `Product` (`productid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `productCategoryCategory`
    FOREIGN KEY (`categoryid`)
    REFERENCES `Category` (`categoryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;