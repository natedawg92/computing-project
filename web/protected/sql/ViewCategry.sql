SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `ViewCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ViewCategory` (
  `viewid` INT NOT NULL COMMENT 'View',
  `categoryid` INT NOT NULL COMMENT 'Category',
  INDEX `viewCategoryView` (`viewid` ASC),
  INDEX `viewCategoryCategory` (`categoryid` ASC),
  PRIMARY KEY (`viewid`, `categoryid`),
  CONSTRAINT `viewCategoryView`
    FOREIGN KEY (`viewid`)
    REFERENCES `View` (`viewid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `viewCategoryCategory`
    FOREIGN KEY (`categoryid`)
    REFERENCES `Category` (`categoryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;