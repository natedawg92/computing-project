SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `View`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `View` (
  `viewid` INT NOT NULL AUTO_INCREMENT COMMENT 'View ID',
  `title` VARCHAR(128) NOT NULL COMMENT 'View Title',
  `content` TEXT NOT NULL COMMENT 'View Content',
  `authorID` INT NOT NULL COMMENT 'Author',
  `viewTypeID` INT NOT NULL COMMENT 'View Type',
  PRIMARY KEY (`viewid`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC),
  INDEX `viewAuthor` (`authorID` ASC),
  INDEX `viewViewType` (`viewTypeID` ASC),
  CONSTRAINT `viewAuthor`
    FOREIGN KEY (`authorID`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `viewViewType`
    FOREIGN KEY (`viewTypeID`)
    REFERENCES `ViewType` (`viewTypeid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;