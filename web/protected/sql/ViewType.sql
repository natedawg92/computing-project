SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `ViewType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ViewType` (
  `viewTypeid` INT NOT NULL AUTO_INCREMENT COMMENT 'View Type ID',
  `name` VARCHAR(64) NOT NULL COMMENT 'View Type Name',
  `description` TEXT NOT NULL COMMENT 'View Type Description',
  PRIMARY KEY (`viewTypeid`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for Table `ViewType`
-- -----------------------------------------------------
INSERT INTO `ComputingProjectMk2`.`ViewType` (`viewTypeid`, `name`, `description`) VALUES (1, 'Post', 'Quick Posts such as news or announcements');
INSERT INTO `ComputingProjectMk2`.`ViewType` (`viewTypeid`, `name`, `description`) VALUES (2, 'Page', 'Static Content E.G About Page');
